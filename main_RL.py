import copy
import itertools
import os
import time
from collections import defaultdict

import pytorch_lightning as pl
import torch
import torch.utils.data as data
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem.rdfiltercatalog import FilterCatalogParams, FilterCatalog
from torch import nn

from dataset import SmilesDataset, MCNNInput
from generate import draw_mol
from main import Model, general_args


class RL_Criteria(nn.Module):
    def forward(self, logits, target):
        # Extracts the normalized logits (corresponding to the probability) for the outcomes we observed.
        return torch.gather(logits - logits.logsumexp(dim=-1, keepdim=True), dim=-1, index=target[..., None])

def steric_strain_filter(mol, cutoff=0.82, max_attempts_embed=20, max_num_iters=200):
    """
    Flags molecules based on a steric energy cutoff after max_num_iters
    iterations of MMFF94 forcefield minimization. Cutoff is based on average
    angle bend strain energy of molecule
    :param mol: rdkit mol object
    :param cutoff: kcal/mol per angle . If minimized energy is above this
    threshold, then molecule fails the steric strain filter
    :param max_attempts_embed: number of attempts to generate initial 3d
    coordinates
    :param max_num_iters: number of iterations of forcefield minimization
    :return: True if molecule could be successfully minimized, and resulting
    energy is below cutoff, otherwise False
    """ # Note this was taken from GraphAF
    # check for the trivial cases of a single atom or only 2 atoms, in which
    # case there is no angle bend strain energy (as there are no angles!)
    if mol.GetNumAtoms() <= 2:
        return True

    # make copy of input mol and add hydrogens
    m = copy.deepcopy(mol)
    m_h = Chem.AddHs(m)

    # generate an initial 3d conformer
    try:
        flag = AllChem.EmbedMolecule(m_h, maxAttempts=max_attempts_embed)
        if flag == -1:
            # print("Unable to generate 3d conformer")
            return False
    except: # to catch error caused by molecules such as C=[SH]1=C2OC21ON(N)OC(=O)NO
        # print("Unable to generate 3d conformer")
        return False

    # set up the forcefield
    AllChem.MMFFSanitizeMolecule(m_h)
    if AllChem.MMFFHasAllMoleculeParams(m_h):
        mmff_props = AllChem.MMFFGetMoleculeProperties(m_h)
        try:    # to deal with molecules such as CNN1NS23(=C4C5=C2C(=C53)N4Cl)S1
            ff = AllChem.MMFFGetMoleculeForceField(m_h, mmff_props)
        except:
            # print("Unable to get forcefield or sanitization error")
            return False
    else:
        # print("Unrecognized atom type")
        return False

    # minimize steric energy
    try:
        ff.Minimize(maxIts=max_num_iters)
    except:
        # print("Minimization error")
        return False

    # ### debug ###
    # min_e = ff.CalcEnergy()
    # print("Minimized energy: {}".format(min_e))
    # ### debug ###

    # get the angle bend term contribution to the total molecule strain energy
    mmff_props.SetMMFFBondTerm(False)
    mmff_props.SetMMFFAngleTerm(True)
    mmff_props.SetMMFFStretchBendTerm(False)
    mmff_props.SetMMFFOopTerm(False)
    mmff_props.SetMMFFTorsionTerm(False)
    mmff_props.SetMMFFVdWTerm(False)
    mmff_props.SetMMFFEleTerm(False)

    ff = AllChem.MMFFGetMoleculeForceField(m_h, mmff_props)

    min_angle_e = ff.CalcEnergy()
    # print("Minimized angle bend energy: {}".format(min_angle_e))

    # find number of angles in molecule
    # from molecule... This is too hacky
    num_atoms = m_h.GetNumAtoms()
    atom_indices = range(num_atoms)
    angle_atom_triplets = itertools.permutations(atom_indices, 3)  # get all
    # possible 3 atom indices groups. Currently, each angle is represented by
    #  2 duplicate groups. Should remove duplicates here to be more efficient
    double_num_angles = 0
    for triplet in list(angle_atom_triplets):
        if mmff_props.GetMMFFAngleBendParams(m_h, *triplet):
            double_num_angles += 1
    num_angles = double_num_angles / 2  # account for duplicate angles

    # print("Number of angles: {}".format(num_angles))

    avr_angle_e = min_angle_e / num_angles

    # print("Average minimized angle bend energy: {}".format(avr_angle_e))

    # ### debug ###
    # for i in range(7):
    #     termList = [['BondStretch', False], ['AngleBend', False],
    #                 ['StretchBend', False], ['OopBend', False],
    #                 ['Torsion', False],
    #                 ['VdW', False], ['Electrostatic', False]]
    #     termList[i][1] = True
    #     mmff_props.SetMMFFBondTerm(termList[0][1])
    #     mmff_props.SetMMFFAngleTerm(termList[1][1])
    #     mmff_props.SetMMFFStretchBendTerm(termList[2][1])
    #     mmff_props.SetMMFFOopTerm(termList[3][1])
    #     mmff_props.SetMMFFTorsionTerm(termList[4][1])
    #     mmff_props.SetMMFFVdWTerm(termList[5][1])
    #     mmff_props.SetMMFFEleTerm(termList[6][1])
    #     ff = AllChem.MMFFGetMoleculeForceField(m_h, mmff_props)
    #     print('{0:>16s} energy: {1:12.4f} kcal/mol'.format(termList[i][0],
    #                                                  ff.CalcEnergy()))
    # ## end debug ###

    if avr_angle_e < cutoff:
        return True
    else:
        return False


def zinc_molecule_filter(mol):
    """
    Flags molecules based on problematic functional groups as
    provided set of ZINC rules from
    http://blaster.docking.org/filtering/rules_default.txt.
    :param mol: rdkit mol object
    :return: Returns True if molecule is okay (ie does not match any of
    therules), False if otherwise
    """ # Note this was taken from GraphAF
    params = FilterCatalogParams()
    params.AddCatalog(FilterCatalogParams.FilterCatalogs.ZINC)
    catalog = FilterCatalog(params)
    return not catalog.HasMatch(mol)



class ReferenceModelDataset(data.Dataset):
    # An accelerated sample, like the one in generate.py, but for reinforcement learning.
    def __init__(self, reference, sample_size, property='plogp', max_nodes=None, temperature=1):
        self.reference = reference
        self.sample_size = sample_size
        self.property = property

        if max_nodes is not None:
            self.max_nodes = max_nodes
        else:
            self.max_nodes = self.reference.max_num_nodes
        self.temperature = temperature
        self.reference.eval()
        # Sample size should be something like 16 * 10 if we take 10 steps before synchronizing networks.

    def rate_molecule(self, smile, bad):
        final_mol = Chem.MolFromSmiles(smile)
        if bad or final_mol.GetNumAtoms() == 0:
            return -3, float('-inf')

        score = 0
        # mol filters with negative rewards
        if steric_strain_filter(final_mol):
            # passes 3D conversion, no excessive strain.
            score -= 1
        if not zinc_molecule_filter(final_mol):
            # does not contain any problematic functional groups.
            score -= 1

        property_score = SmilesDataset.properties[self.property](final_mol)
        factor = 1
        if self.property == 'qed':
            factor = 10  # This factor turned out to help a lot :)

        return score + property_score * factor, property_score

    def __len__(self):
        return 1

    def __getitem__(self, _):
        properties = None
        # properties = torch.tensor([(271.485, 1.8487)], dtype=torch.float, device=device)

        molecules, log_prob, bad, node_types, edge_types, graph_size = \
            self.reference.batch_sample(self.sample_size,
                                        self.max_nodes, self.temperature,
                                        properties=properties, return_graph=True, ensure_connected=True)
        validity_masks = self.reference.get_validity_masks(node_types, edge_types, self.reference.edge_pred_ji, False)
        scores = []
        property_scores = []
        smiles = molecules
        for i in range(self.sample_size):
            score, property_score = self.rate_molecule(smiles[i], bad[i])
            scores.append(score)
            property_scores.append(property_score)

        rewards = torch.tensor(scores, dtype=torch.float)
        property_scores = torch.tensor(property_scores, dtype=torch.float)

        return node_types, edge_types, graph_size, rewards, property_scores, validity_masks, smiles



class ModelRL(Model):
    def __init__(self, hparams):
        super().__init__(hparams) # (dataloader, unique_smiles, args)
        self.model.criteria = RL_Criteria()  # Replace critaria.
        self.old_model = [copy.deepcopy(self.model)] # Reference model for PPO
        self.old_model[0].eval()
        for parameter in self.old_model[0].parameters():
            parameter.requires_grad = False

        self.start_time = time.time()
        # Replace datasets with dummy datasets in case there is no prior! (Prevent slow multiprocessing if not needed.)
        self.best_molecules = []
        self.best = None
        self.number_of_samples = 0

        self.baseline = [nn.BatchNorm1d(1, affine=False)]  # Higher eps, so we don't blow up tiny differences.

        self.step = 0

        self.rl_dataset = None
        # self.sample_rl_dataset(self.hparams.batch_size * self.update_iters)
        self.hparams.reload_dataloaders_every_epoch = True  # For RL Specifically.

    def train_dataloader(self):
        device = next(self.model.parameters()).device
        if self.model.max_num_nodes != self.hparams.gen_max_nodes:
            self.model.max_num_nodes = self.hparams.gen_max_nodes
            self.model.setup_masks(self.hparams.gen_max_nodes)
            self.model = self.model.to(device)  # Move masks
            self.old_model[0].setup_masks(self.hparams.gen_max_nodes)  # Prevent error loading state dict.

        # Reset optimizer state. (I.e. set momentum to zero, so we don't overshoot.)
        # Would be simpler to maybe just not use Adam and use SGD instead, but this works.
        self.trainer.optimizers[0].__setstate__(self.trainer.optimizers[0].__getstate__()['defaults'])
        self.trainer.optimizers[0].state = defaultdict(dict)

        self.baseline[0] = self.baseline[0].to(device)  # In case it's not on the same device.
        self.old_model[0].load_state_dict(self.model.state_dict())
        ds = data.ConcatDataset([ReferenceModelDataset(self.old_model[0], self.hparams.batch_size,
                                                       self.hparams.property, max_nodes=self.hparams.gen_max_nodes)]
                                * self.hparams.update_iters)
        return data.DataLoader(ds, batch_size=1, num_workers=self.hparams.num_workers, shuffle=False, pin_memory=True)

    def val_dataloader(self):
        pass

    def validation_step(self, batch, batch_idx):
        pass

    def validation_epoch_end(self, outputs):
        # Maybe sample the self.model and see if it's actually better.
        print('The best molecules generated so far are')
        print(self.best_molecules)
        newline = "  \n"
        self.logger.experiment.add_text('RL/best', f'<pre>{newline.join(["Optimizing " + self.hparams.property + ", samples:" + str(self.number_of_samples)] + list(map(str, self.best_molecules)))}</pre>',
                                        global_step=self.global_step)
        return {'val_loss': 0} # torch.tensor(-self.best[0] if self.best is not None else 0)

    def training_step(self, batch, batch_idx):

        node_type, edge_type, graph_size, rewards, property_scores, validity_masks = map(lambda t: t[0], batch[:-1])
        molecules = list(itertools.chain.from_iterable(batch[-1]))
        sample = MCNNInput(node_type=node_type, adj_type=edge_type,
                           graph_size=graph_size, smile=molecules,
                           properties=None, validity_masks=validity_masks)
        self.model.eval()
        #for module in self.model.modules():
        #    if isinstance(module, torch.nn.modules.BatchNorm1d) \
        #            or isinstance(module, torch.nn.modules.Dropout):
        #        module.eval()
        self.number_of_samples += len(molecules)
        node_logits, edge_logits = self.model(sample.node_type, sample.adj_type, sample.properties, mode='both',
                                              validity_masks=sample.validity_masks)
        ll_nodes, ll_edges = self.model.loss(node_logits, edge_logits, sample.node_type, sample.adj_type,
                                             sample.graph_size, reduce=False)

        # self.bline[0].to(rewards.device)
        # advantage = self.bline[0](rewards[:, None])[:, 0]  # Expand the one feature into a "channel" dimension.
        print("Generated mols:", set(molecules))
        target_values = []
        already_seen = torch.zeros((len(molecules),), dtype=torch.float)
        mproperties = property_scores[~torch.isinf(property_scores)].mean()
        sproperties = property_scores[~torch.isinf(property_scores)].std()
        for idx, (score, mol) in enumerate(zip(property_scores.cpu(), molecules)):
            if self.hparams.property == 'qed':
                if mol in self.all_train_smiles:
                    print('Already seen', mol)
                    rewards[idx] = mproperties - sproperties * 1.  # Punish.
                    already_seen[idx] = True

                if torch.isinf(score):
                    rewards[idx] = mproperties - sproperties * 2.  # Invalid molecule in some way. Punish.
                    # ^ This is needed for QED optimization, since otherwise the -2 absolute punishment for invalid mols
                    #    is simply too large when QED spans [0, 1], so it blows up the baseline running variance.
                    continue

            self.all_train_smiles.add(mol)
            if self.best is None:
                self.best = (score, mol)
            if self.best < (score, mol):
                # New best.
                self.best_molecules.append((score.item(), mol))
                bestmol = Chem.MolFromSmiles(mol)
                drawing = draw_mol(bestmol, legend=f'{score:.3f}',
                                   legendsize=20, indices=False, kekulize=True)
                print(drawing.GetDrawingText(),
                      file=open(os.path.join(os.path.join(self.trainer.ckpt_path, '..', f'{len(self.best_molecules)}_{time.time() - self.start_time:.0f}.svg')),
                                'w+'))
            self.best = max(self.best, (score, mol))
            target_values.append(score)
        if self.best is not None:
            print(f'Best is {self.best[0]:.3f}: {self.best[1]}')

        advantage = self.baseline[0](rewards[:, None])[:, 0]  # - 2 * already_seen.to(rewards.device) # Expand the one feature into a "channel" dimension.

        print(rewards.mean().item(), advantage.mean().item(),
              property_scores[~torch.isinf(property_scores)].mean().item(), property_scores.max().item())

        # Normalization
        num_masks = (graph_size  # Number of nodes.
                     + ((self.model.max_graph_bandwidth - 1) * self.model.max_graph_bandwidth) // 2
                     + (graph_size - self.model.max_graph_bandwidth) * graph_size)
        num_mask_edge = num_masks - graph_size
        node_total_length = graph_size * len(self.model.node_types)
        node_total_length[node_total_length == 0] = 1  # In case an empty graph was generated.
        edge_total_length = num_mask_edge * len(self.model.edge_types)

        likelihoods = (ll_nodes.sum(-1) + ll_edges.sum(-1)) / (edge_total_length + node_total_length)
        # likelihoods = ll_nodes.sum(-1) + ll_edges.sum(-1)
        expectation = likelihoods.exp() * advantage
        loss = -expectation.mean(0)
        # print(ll_nodes.sum(-1) + ll_edges.sum(-1), likelihoods, (likelihoods - likelihoods.logsumexp(0)).exp())
        log = {'Loss/rl': loss}
        mean_score = 0
        if len(target_values):
            mean_score = sum(target_values) / len(target_values)
            log['Loss/rl_score'] = mean_score
        if self.best is not None:
            log['Loss/rl_best'] = self.best[0]
        return {'loss': loss * self.hparams.accumulate_grad_batches,
                'log': log,
                'progress_bar': {'mean_score': mean_score,
                                 'bn_mean': self.baseline[0].running_mean,
                                 'bn_std': torch.sqrt(self.baseline[0].running_var)}}

    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = Model.add_model_specific_args(parent_parser)
        # Finetune parameters
        parser.add_argument('--iters_finetune', default=10)  # Currently not used - epochs in Lightning?
        parser.add_argument('--update_iters', default=128, type=int)
        parser.add_argument('--property', default='plogp')
        # parser.add_argument('--reward_type', default='linear')
        # parser.add_argument('--warmup', default=0)  # Tjek om den skal bruges
        # parser.add_argument('--lr_decay', default=True)
        parser.add_argument('--save_path', default='./saved_molecules/')
        return parser


if __name__ == "__main__":
    parser = general_args()
    # Hyperparameters
    parser = ModelRL.add_model_specific_args(parser)

    parser = pl.Trainer.add_argparse_args(parser)  # Add trainer options.
    args = parser.parse_args()
    model = ModelRL(args)
    trainer = pl.Trainer.from_argparse_args(args)

    trainer.fit(model)