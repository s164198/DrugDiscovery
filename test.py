import rdkit

import torch

from moses.char_rnn import CharRNNTrainer, CharRNN
from moses.char_rnn.config import get_parser
from moses.script_utils import add_train_args, set_seed
from rdkit import Chem
import rdkit.Chem.rdchem as rdchem
from tqdm import tqdm
import pandas as pd

import moses
from torch.utils.data import DataLoader
from moses import CharVocab, StringDataset

import numpy as np
import scipy


import os
import re

def convert_radical_electrons_to_hydrogens(mol):
    """
    Converts radical electrons in a molecule into bonds to hydrogens. Only
    use this if molecule is valid. Results a new mol object
    :param mol: rdkit mol object
    :return: rdkit mol object
    """
    m = copy.deepcopy(mol)
    if Chem.Descriptors.NumRadicalElectrons(m) == 0:  # not a radical
        return m
    else:  # a radical
        print('converting radical electrons to H')
        for a in m.GetAtoms():
            num_radical_e = a.GetNumRadicalElectrons()
            if num_radical_e > 0:
                a.SetNumRadicalElectrons(0)
                a.SetNumExplicitHs(num_radical_e)
    return m

def process_mol(mol):
    ### convert smiles to the molecular form we want
    Chem.SanitizeMol(mol)
    Chem.Kekulize(mol)
    Chem.RemoveStereochemistry(mol)  # (- In case it is present)
    mol = Chem.RemoveHs(mol, sanitize=False)

    # From https://www.rdkit.org/docs/GettingStartedInPython.html:
    # "Note: as of this writing (Aug 2008), the smiles provided when one requests kekuleSmiles are not canonical.
    # The limitation is not in the SMILES generation, but in the kekulization itself."
    return mol

def reorder(adj_type):
    # Reverse Cuthill-McKee
    csc = scipy.sparse.csc_matrix(adj_type)
    order = np.ascontiguousarray(scipy.sparse.csgraph.reverse_cuthill_mckee(csc, symmetric_mode=True))  # [::-1]
    return order


def smiles_to_string(smiles, full=True, separator=''):
    #TODO: Maybe allow SMILES-syntax for aromatic rings?
    # Can also try full=True, separator=' ' for more explicit output.
    mol = Chem.MolFromSmiles(smiles)
    Chem.SanitizeMol(mol)
    canonical_order = list(Chem.CanonicalRankAtoms(mol))
    Chem.Kekulize(mol)
    Chem.RemoveStereochemistry(mol)  # (- In case it is present)
    mol = Chem.RemoveHs(mol, sanitize=False)

    # Extract the graph
    atoms = [atom.GetSymbol() for atom in mol.GetAtoms()]
    adj_type = Chem.GetAdjacencyMatrix(mol, useBO=True)

    # Reorder
    # order = np.flipud(reorder(adj_type))

    key = lambda atom: (atom.GetDegree(), canonical_order[atom.GetIdx()])  # Sign? mb. include atomicnumber or valency?

    x = min(mol.GetAtoms(), key=key)
    order = []
    queue = [x]
    R = {x.GetIdx()}
    # print('x:', x.GetSymbol(), canonical_order[x.GetIdx()])
    while queue:
        x = queue.pop(0)
        for a in sorted(x.GetNeighbors(), key=key):
            if a.GetIdx() not in R:
                queue.append(a)
                R.add(a.GetIdx())
        order.append(x.GetIdx())

    # Done. Ready to encode.
    adj_type = adj_type[np.ix_(order, order)]

    code = ''
    for i in range(len(atoms)):
        code += atoms[order[i]]
        nz = adj_type[i, :i].nonzero()[0]
        if len(nz):
            width = (i - nz).max()

            # We use int to convert to integer (requires kekulization!)
            addition = ''.join(str(int(x)) for x in np.flipud(adj_type[i, i - width:i]))
            if not full and (addition.endswith('01') or addition == '1'):
                # In this case shorten it.
                addition = addition[:-1]
            code += addition

        if i != len(atoms) - 1:
            code += separator

    return code

def string_to_smiles(string):
    bond_types = (0, rdchem.BondType.SINGLE, rdchem.BondType.DOUBLE, rdchem.BondType.TRIPLE)  # 0 for NO_BOND.

    rw_mol = Chem.RWMol()
    # Assuming the string was created with full=True, we can simply split it using digits.
    # for atom_idx, atom_str in enumerate(string.split()):
    for atom_idx, atom_str in enumerate(re.findall('[A-Z][a-z]*\d*', string)):
        digit_pos = len(atom_str)
        m = re.search(r"\d", atom_str)
        if m is not None:
            digit_pos = m.start()

        atom_name = atom_str[:digit_pos]
        rw_mol.AddAtom(Chem.Atom(atom_name))

        if atom_idx == 0:
            continue

        bonds = atom_str[digit_pos:]
        if bonds.endswith('0') or len(bonds) == 0:
            bonds += '1'  # Re-add implicit single bonds.

        for bond_pos, bond_type in enumerate(bonds, start=1):
            bond_type = bond_types[int(bond_type)]
            if bond_type:
                rw_mol.AddBond(atom_idx - bond_pos, atom_idx, bond_type)

    mol = rw_mol.GetMol()

    final_mol = convert_radical_electrons_to_hydrogens(mol)
    Chem.SanitizeMol(final_mol)
    smile = Chem.MolToSmiles(final_mol, isomericSmiles=False)

    return smile



if __name__ == "__main__":
    train = moses.get_dataset('train')
    data_path = os.path.join('dataset', 'train.csv.gz')
    if not os.path.exists(data_path):
        train_strings = np.array([smiles_to_string(smiles) for smiles in tqdm(train)])
        pd.DataFrame(data=train_strings, columns=["SMILES"]).to_csv(data_path, compression='gzip', index=False)
    else:
        train_strings = pd.read_csv(data_path, compression='gzip')['SMILES'].values

    vocab = CharVocab.from_data(train)


    full_size = len(train_strings)
    val_size = int(0.05 * full_size)
    train_size = full_size - val_size

    # We want the test set to stay fixed even if we change the seed.
    # print(list(vocab.c2i.keys()))

    vocab = CharVocab.from_data(train_strings)

    config = add_train_args(get_parser()).parse_args()

    device = torch.device(config.device)

    if config.config_save is not None:
        torch.save(config, config.config_save)

    set_seed(config.seed)
    train_strings, val_strings = torch.utils.data.random_split(train_strings, [train_size, val_size])


    trainer = CharRNNTrainer(config)
    model = CharRNN(vocab, config).to(device)

    trainer.fit(model, train_strings, val_strings)
    model = model.to('cpu')
    torch.save(model.state_dict(), config.model_save)
    # print(list(vocab.c2i.keys()))
    exit()
    vocab = CharVocab.from_data(train)
    train_dataset = StringDataset(vocab, train)
    train_dataloader = DataLoader(
        train_dataset, batch_size=512,
        shuffle=True, collate_fn=train_dataset.default_collate
    )

    # x, y = zip(*[(len(train[i]), len(smiles_to_string(train[i]))) for i in range(3000)])
    # import seaborn as sns
    # import matplotlib.pyplot as plt

    # sns.jointplot(x, y, kind='kde', ratio=1)
    # plt.show()

    dtx = 4

    # It works like a stack (with implied single-bonds!)
    # E.g. C02 means pop/skip one in stack, then double bond to the one two back.
    # Maybe data can travel directly along the stack, and the connections as well.
    # The reason to use this order is because then we wont have to specify the (band)width,
    #  - the set of bonds start from the current atom and goes back in the past for each token.
    #  - if it was done reversed, then we would not know how many 0's to pad with.
    # One can use bidirectional RNN to traverse this stack and backtrack, but also try using attention for perf.

    # print(code, len(code), train[dtx], len(train[dtx]))

    #for with_bos, with_eos, lengths in train_dataloader:
    #    print(with_bos, with_eos, lengths)
    #    exit()
