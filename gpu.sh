#!/bin/sh
### General options
### â€“- specify queue --
#BSUB -q gpuv100
### -- set the job Name --
#BSUB -J MCNN
### -- ask for number of cores (default: 1) --
#BSUB -n 12
### -- Select the resources: 1 gpu in exclusive process mode -- BSUB -gpu "num=1:mode=exclusive_process"
#BSUB -gpu "num=1"
### -- set walltime limit: hh:mm --  maximum 24 hours for GPU-queues right now
#BSUB -W 23:59
# request 5GB of system-memory per core
#BSUB -R "rusage[mem=8GB]"
### -- set the email address --
# please uncomment the following line and put in your e-mail address,
# if you want to receive e-mail notifications on a non-default address
##BSUB -u your_email_address
### -- send notification at start --
#BSUB -B
### -- send notification at completion--
#BSUB -N
### -- Specify the output and error file. %J is the job-id --
### -- -o and -e mean append, -oo and -eo mean overwrite --
#BSUB -o gpu-%J.out
#BSUB -e gpu_%J.err
# -- end of LSF options --
nvidia-smi
python3 -c 'import torch; print(torch.cuda.is_available())'
python3 main.py --num_workers=12 --val_check_interval=0.25 --gpus=0 --batch_size=32 --log_level=1 --row_log_interval=100 --hidden_size=1024 --conditioning qed
