import numpy as np
import networkx as nx
import torch
import torch_sparse
import pickle
from torch.utils.data import Dataset
from rdkit import Chem
import torch.nn
import torch.nn as nn
import pandas as pd
import rdkit.Chem.rdchem as rdchem
from PIL import Image
from rdkit.Chem import Descriptors
from rdkit.Chem import QED
import math
from rdkit.Chem import rdMolDescriptors
from rdkit.Chem.Draw import IPythonConsole
from rdkit.Chem import Draw
from rdkit import Chem
from rdkit.Chem import rdDepictor
from rdkit.Chem.Draw import rdMolDraw2D
import scipy.sparse as sps
import scipy.sparse
import pickle
from tqdm import tqdm
import gzip


def smile_to_matrix(smile):
    m = Chem.MolFromSmiles(smile)
    im = Draw.MolToImage(m)
    M = np.array(im) # OBS - this is saved as a RGBA!
    return M

smile = 'C1CSCCSCCS1'
M = smile_to_matrix(smile)
img = Image.fromarray(M,'RGBA')
img.show()