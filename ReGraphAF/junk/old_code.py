

#### Make functions for preprocessing - returns X and A
def transform_smile(smile):
    ## Returns X and A from smile
    # Convert molecyle and  kekulize
    m = Chem.MolFromSmiles(smile)
    Chem.Kekulize(m)

    n = m.GetNumAtoms()

    # Make map for atoms
    nums = [6, 7, 8, 9, 15, 16, 17, 35, 53, 0] # C N O F P S Cl Br I
    vals = np.arange(len(nums))
    map_ = dict(zip(nums,vals))


    # Make X
    X = torch.zeros(n,len(nums))
    atoms = [map_[atom.GetAtomicNum()] for atom in m.GetAtoms()]
    X[np.arange(len(atoms)),atoms] = 1


    # Make A
    map_ = {Chem.BondType.SINGLE: 0,Chem.BondType.DOUBLE: 1,Chem.BondType.TRIPLE: 2}
    A = torch.zeros((3, n, n))
    bond_type = [map_[bond.GetBondType()] for bond in m.GetBonds()]
    from_ = [bond.GetBeginAtomIdx() for bond in m.GetBonds()]
    to_ = [bond.GetEndAtomIdx() for bond in m.GetBonds()]

    A[bond_type, from_, to_] = 1

    return X,A


### If debugging:
if sys.argv[1] == 'debug':
    f = open("debugadj.pkl", "rb")
    adj = pickle.load(f)
    f.close()

    f = open("debugnodes.pkl", "rb")
    nodes = pickle.load(f)
    f.close()

    f = open("debug_mol_sizes.pkl", "rb")
    mol_sizes = pickle.load(f)
    f.close()
    args['save_path'] = 'debug_models'

# Full data
else:
    f = open("../data_processed/_adj.pkl", "rb")
    adj = pickle.load(f)
    f.close()
    f = open("../data_processed/_nodes.pkl", "rb")
    nodes = pickle.load(f)
    f.close()
    f = open("../data_processed/_mol_sizes.pkl", "rb")
    mol_sizes = pickle.load(f)
    f.close()