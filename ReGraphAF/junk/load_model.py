from model import *
from dataset import *


def restore_model(model, epoch = None):
    if epoch is None:
        restore_path = os.path.join('../saved_models', 'checkpoint')
        print('Restore from the latest checkpoint')
    else:
        restore_path = os.path.join('../saved_models', 'checkpoint%s' % str(epoch))
        print('restore from checkpoint%s' % str(epoch))

    if torch.cuda.is_available():
        checkpoint = torch.load(restore_path)
    else:
        checkpoint = torch.load(restore_path,map_location=torch.device('cpu'))
    state_dict = checkpoint['model_state_dict']
    model.load_state_dict(checkpoint['model_state_dict'])


model = MolecularCNN()
restore_model(model)
