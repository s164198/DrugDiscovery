
# Lazy solution
from dataset import *

## Different bond types
## Moses {'SINGLE': 99999, 'AROMATIC': 98361, 'DOUBLE': 88799, 'TRIPLE': 7860}
## Zinc {'SINGLE': 99993, 'AROMATIC': 96945, 'DOUBLE': 88438, 'TRIPLE': 3472}
## QM9 {'SINGLE': 99630, 'AROMATIC': 16449, 'DOUBLE': 47932, 'TRIPLE': 24253}

## All bonds
## Moses {'SINGLE': 1069585, 'AROMATIC': 1105736, 'DOUBLE': 141781, 'TRIPLE': 7924}
## Zinc {'SINGLE': 1349062, 'AROMATIC': 1184764, 'DOUBLE': 179435, 'TRIPLE': 3622}
## QM9 {'SINGLE': 762721, 'AROMATIC': 89404, 'DOUBLE': 60381, 'TRIPLE': 27691}

## Different atomic types
## Moses {'Br': 3247, 'C': 100000, 'N': 99065, 'O': 96432, 'F': 18718, 'Cl': 10499, 'S': 32032}
## Zinc {'C': 100000, 'N': 98121, 'O': 95715, 'S': 38039, 'Br': 4156, 'F': 16941, 'Cl': 14364, 'I': 434, 'P': 79}
## QM9 {'C': 99999, 'N': 61780, 'O': 85045, 'F': 1532}

## All atoms
## Moses {'N': 296305, 'C': 1563499, 'S': 35710, 'O': 224654, 'F': 30618, 'Cl': 11909, 'Br': 3259}
## Zinc {'O': 268922, 'C': 1842636, 'N': 302871, 'S': 44970, 'F': 28457, 'Br': 4313, 'Cl': 16858, 'I': 440, 'P': 81}
## QM9 {'C': 632761, 'O': 140564, 'N': 103773, 'F': 2409}


### Save output setting atoms >=5

#Time of generating (10000/10778) molecules(#atoms>=5): 1677.57119 | unique rate: 0.75680 | valid rate: 0.70710 | novelty: 0.99990
#writing 10000 smiles into ./mols/test_100mol.txt done!


## Pytorch

## Rdkit
from rdkit import Chem
from rdkit.Chem import Descriptors
from rdkit.Chem import QED
from rdkit.Chem import rdMolDescriptors
import rdkit.Chem.rdchem as rdchem
import scipy

## Other
from tqdm import tqdm
import gzip
import math
import os
import pickle
import networkx as nx
import numpy as np
import pandas as pd


def calculateScore(m, _fscores):
    # fragment score
    fp = rdMolDescriptors.GetMorganFingerprint(m, 2)  # <- 2 is the *radius* of the circular fingerprint
    fps = fp.GetNonzeroElements()
    score1 = 0.
    nf = 0
    for bitId, v in fps.items():
        nf += v
        sfp = bitId
        score1 += _fscores.get(sfp, -4) * v
    score1 /= nf
    # features score
    nAtoms = m.GetNumAtoms()
    nChiralCenters = len(Chem.FindMolChiralCenters(m, includeUnassigned=True))
    ri = m.GetRingInfo()
    nBridgeheads, nSpiro = SmilesDataset.numBridgeheadsAndSpiro(m, ri)
    nMacrocycles = 0
    for x in ri.AtomRings():
        if len(x) > 8:
            nMacrocycles += 1

    sizePenalty = nAtoms ** 1.005 - nAtoms
    stereoPenalty = math.log10(nChiralCenters + 1)
    spiroPenalty = math.log10(nSpiro + 1)
    bridgePenalty = math.log10(nBridgeheads + 1)
    macrocyclePenalty = 0.
    # ---------------------------------------
    # This differs from the paper, which defines:
    #  macrocyclePenalty = math.log10(nMacrocycles+1)
    # This form generates better results when 2 or more macrocycles are present
    if nMacrocycles > 0:
        macrocyclePenalty = math.log10(2)

    score2 = 0. - sizePenalty - stereoPenalty - spiroPenalty - bridgePenalty - macrocyclePenalty

    # correction for the fingerprint density
    # not in the original publication, added in version 1.1
    # to make highly symmetrical molecules easier to synthetise
    score3 = 0.
    if nAtoms > len(fps):
        score3 = math.log(float(nAtoms) / len(fps)) * .5

    sascore = score1 + score2 + score3

    # need to transform "raw" value into scale between 1 and 10
    min = -4.0
    max = 2.5
    sascore = 11. - (sascore - min + 1) / (max - min) * 9.
    # smooth the 10-end
    if sascore > 8.:
        sascore = 8. + math.log(sascore + 1. - 9.)
    if sascore > 10.:
        sascore = 10.0
    elif sascore < 1.:
        sascore = 1.0

    return sascore


class Statistics():
    def __init__(self, smi_path,bfs = False):
        self.file_smi = '.smi'
        self.file_pickle = 'stat.pickle'

        pickle_path = smi_path[:-len(self.file_smi)] + self.file_pickle

        self.bfs = False


        ### Initiate _fscores for SA score function to work
        data = pickle.load(gzip.open('fpscores.pkl.gz'))
        outDict = {}
        for i in data:
            for j in range(1, len(i)):
                outDict[i[j]] = float(i[0])
        self._fscores = outDict

        print(pickle_path)
        if not os.path.exists(pickle_path) or os.path.getmtime(pickle_path) < os.path.getmtime(smi_path):
            try:
                # Python principle: Ask forgiveness not permission
                print('Pickle file older than smile file - deleting pickle file')
                os.remove(pickle_path)
            except FileNotFoundError:
                pass
                print('Found no pickle file - processing smile files to pickle file')

            self.process(smi_path)

        self.__dict__update(pickle_path)

    def __dict__update(self,pickle_path):

        data = pickle.load(open(pickle_path, "rb"))
        self.smiles = data['smiles']
        self.n_molecule = len(self.smiles)
        atoms = data['atoms']
        bonds = (None, rdchem.BondType.SINGLE, rdchem.BondType.DOUBLE, rdchem.BondType.TRIPLE)
        self.atoms = {atom: idx for idx, atom in enumerate(atoms)}
        self.bonds = {bond: idx for idx, bond in enumerate(bonds)}
        self.max_size = data['max_atoms']  # Set to this due to padding issues
        self.diameter = data['diameter']


    def __len__(self):
        return self.n_molecule

    def process(self,smi_path):
        smiles = pd.read_csv(smi_path, sep='\n', header=None)[0].values  # Pandas seems to be better for loading (less lag)

        self.n_molecule = len(smiles)

        all_smiles = []
        max_atoms = 0
        atoms = set()
        diameter = 0
        logP = np.zeros(self.n_molecule)
        qed = np.zeros(self.n_molecule)
        mw = np.zeros(self.n_molecule)
        SA = np.zeros(self.n_molecule)
        cycle = np.zeros(self.n_molecule)

        # Dictionary for different bond types and atomic types
        bond_atom = {'SINGLE': 0, 'AROMATIC': 0, 'DOUBLE': 0, 'TRIPLE': 0}
        bond_all = {'SINGLE': 0, 'AROMATIC': 0, 'DOUBLE': 0, 'TRIPLE': 0}
        atom_dict = {}
        atom_all = {}



        for i, smile in enumerate(tqdm(smiles)):
            m = Chem.MolFromSmiles(smile)
            m = SmilesDataset.process_mol(m)
            ### Find unique atoms
            ATOMS = m.GetAtoms()

            for atom in ATOMS:
                atoms.add(atom.GetSymbol())

            # Check if it is larger
            max_atoms = len(ATOMS) if max_atoms < len(ATOMS) else max_atoms

            # Calculate diameter
            adjmat = Chem.GetAdjacencyMatrix(m, useBO=True)

            # BFS
            if self.bfs:
                G = nx.from_numpy_matrix(adjmat)
                bfs_perm = np.array(SmilesDataset.bfs_seq(G, 0))  # Just set 0 to the start idx
                adjmat_bfs = np.array(adjmat[np.ix_(bfs_perm, bfs_perm)])
                foo, bar = np.where(adjmat_bfs > 0)  # rows and cols where nonzeros are located
                dist = max(foo - bar) + 1  # add 1 for correction
            else:
            # Calculate diameter with reverse cuthill mckee
                adjmat = Chem.GetAdjacencyMatrix(m, useBO=True)
                csc = scipy.sparse.csc_matrix(adjmat)
                order = scipy.sparse.csgraph.reverse_cuthill_mckee(csc)
                adjmat_rcm = np.array(adjmat[np.ix_(order, order)])
                foo, bar = np.where(adjmat_rcm > 0)  # rows and cols where nonzeros are located
                dist = max(foo - bar) + 1  # add 1 for correction

            diameter = dist if dist > diameter else diameter

            # Calculate bond types in each atom
            unique_bond = np.unique(adjmat)
            if 1 in unique_bond or 1.5 in unique_bond:
                bond_atom['SINGLE'] += 1
            if 1.5 in unique_bond:
                bond_atom['AROMATIC'] += 1
            if 2 in unique_bond or 1.5 in unique_bond:
                bond_atom['DOUBLE'] += 1
            if 3 in unique_bond:
                 bond_atom ['TRIPLE'] += 1

            # Calculate ALL bond types
            bonds = m.GetBonds()
            for bond in bonds:
                bt = bond.GetBondType().name
                bond_all[bt]  += 1

            # Get all atoms
            all_atoms = [atom.GetSymbol() for atom in ATOMS]
            for atom in all_atoms:
                if atom in atom_all:
                    atom_all[atom] +=1
                else:
                    atom_all[atom] = 1

            # Get unique atoms
            unique_atoms = np.unique([atom.GetSymbol() for atom in ATOMS])
            for atom in unique_atoms:
                if atom in atom_dict:
                    atom_dict[atom] +=1
                else:
                    atom_dict[atom] = 1

            qed[i] = QED.qed(m)
            mw[i] = rdMolDescriptors.CalcExactMolWt(m)
            logP[i] = Descriptors.MolLogP(m)
            SA[i] = calculateSA(m)
            cycle[i] = cyclescore(m)

            # Convert back
            s = Chem.MolToSmiles(m)
            all_smiles.append(s)

        diameter = int(diameter)

        # Order atoms and turn them to ordered list
        atoms = np.array(list(atoms))
        atom_N = np.array([Chem.GetPeriodicTable().GetAtomicNumber(atom) for atom in atoms])
        atoms = atoms[np.argsort(atom_N)]


        processed_data = {'smiles': smiles,
                          'atoms': atoms,
                          'max_atoms': max_atoms,
                          'diameter': diameter,
                          'qed': qed,
                          'mw': mw,
                          'SA': SA,
                          'logP': logP,
                          'cycle': cycle,
                          'bond_atom': bond_atom,
                          'bond_all': bond_all,
                          'atom_dict': atom_dict,
                          'atom_all': atom_all
                          }
        if self.bfs:
            pickle_path = smi_path[:-len(self.file_smi)]  + 'bfs'+ self.file_pickle  # Change .smi to .pickle
        else:
            pickle_path = smi_path[:-len(self.file_smi)] + self.file_pickle  # Change .smi to .pickle
        with open(pickle_path,"wb") as f:
            pickle.dump(processed_data, f)

if __name__ == "__main__":

    ### Initiate _fscores for SA score function to work
    data = pickle.load(gzip.open('fpscores.pkl.gz'))
    outDict = {}
    for i in data:
        for j in range(1, len(i)):
            outDict[i[j]] = float(i[0])
    _fscores = outDict

    smi_paths = ['dataset/MOSES_dataset_v1_train.smi']

    for smi_path in smi_paths:
        Statistics(smi_path)