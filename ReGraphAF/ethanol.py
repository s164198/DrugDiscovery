
## Pytorch

## Rdkit
from rdkit import Chem

## Other
from PIL import Image
from tqdm import tqdm
import gzip
import math
import os
import pickle
import networkx as nx
import numpy as np
import pandas as pd
import scipy
from tqdm import tqdm


path = 'dataset/250Kzink.smi'
moses = pd.read_csv('dataset/moses.csv',sep='\n').values

ions = dict()

smiles = moses

for i in range(len(smiles)):
    if i % 10000 == 0:
        print(i)
        print(ions)
    smile = smiles[i][0]
    m = Chem.MolFromSmiles(smile)
    for atom in m.GetAtoms():
        charge = atom.GetFormalCharge()

        if charge != 0:
            type = atom.GetSymbol() + str(charge)

            if type in ions:
                ions[type] += 1
            else :
                ions[type] = 1


for i in range(len(smiles)):
    smile = smiles[i][0]
    if '+' in smile:
        print(smile)