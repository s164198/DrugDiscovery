from __future__ import print_function
import numpy as np
import sys
import pickle
from tqdm import tqdm
from rdkit import Chem
from rdkit.Chem import Descriptors
from rdkit.Chem import QED
import math
from rdkit.Chem import rdMolDescriptors
from rdkit import Chem
import gzip

## Function used to calculate SA score - taken from: https://github.com/rdkit/rdkit/blob/master/Contrib/SA_Score/sascorer.py#L133
## Copyright notice for the function calculateScore
#
#  Copyright (c) 2013, Novartis Institutes for BioMedical Research Inc.
#  All rights reserved.
#
from dataset import penalized_logp

data = pickle.load(gzip.open('fpscores.pkl.gz'))
outDict = {}
for i in data:
    for j in range(1, len(i)):
        outDict[i[j]] = float(i[0])
_fscores = outDict

def numBridgeheadsAndSpiro(mol, ri=None):
    nSpiro = rdMolDescriptors.CalcNumSpiroAtoms(mol)
    nBridgehead = rdMolDescriptors.CalcNumBridgeheadAtoms(mol)
    return nBridgehead, nSpiro

def calculateScore(m):

    # fragment score
    fp = rdMolDescriptors.GetMorganFingerprint(m,
                                               2)  # <- 2 is the *radius* of the circular fingerprint
    fps = fp.GetNonzeroElements()
    score1 = 0.
    nf = 0
    for bitId, v in fps.items():
        nf += v
        sfp = bitId
        score1 += _fscores.get(sfp, -4) * v
    score1 /= nf

    # features score
    nAtoms = m.GetNumAtoms()
    nChiralCenters = len(Chem.FindMolChiralCenters(m, includeUnassigned=True))
    ri = m.GetRingInfo()
    nBridgeheads, nSpiro = numBridgeheadsAndSpiro(m, ri)
    nMacrocycles = 0
    for x in ri.AtomRings():
        if len(x) > 8:
            nMacrocycles += 1

    sizePenalty = nAtoms**1.005 - nAtoms
    stereoPenalty = math.log10(nChiralCenters + 1)
    spiroPenalty = math.log10(nSpiro + 1)
    bridgePenalty = math.log10(nBridgeheads + 1)
    macrocyclePenalty = 0.
    # ---------------------------------------
    # This differs from the paper, which defines:
    #  macrocyclePenalty = math.log10(nMacrocycles+1)
    # This form generates better results when 2 or more macrocycles are present
    if nMacrocycles > 0:
        macrocyclePenalty = math.log10(2)

    score2 = 0. - sizePenalty - stereoPenalty - spiroPenalty - bridgePenalty - macrocyclePenalty

    # correction for the fingerprint density
    # not in the original publication, added in version 1.1
    # to make highly symmetrical molecules easier to synthetise
    score3 = 0.
    if nAtoms > len(fps):
        score3 = math.log(float(nAtoms) / len(fps)) * .5

    sascore = score1 + score2 + score3

    # need to transform "raw" value into scale between 1 and 10
    min = -4.0
    max = 2.5
    sascore = 11. - (sascore - min + 1) / (max - min) * 9.
    # smooth the 10-end
    if sascore > 8.:
        sascore = 8. + math.log(sascore + 1. - 9.)
    if sascore > 10.:
        sascore = 10.0
    elif sascore < 1.:
        sascore = 1.0

    return sascore


def get_all_bonds(smiles):
    print('Getting all bonds')
    bond_dict = {'SINGLE': 0, 'AROMATIC': 0, 'DOUBLE': 0, 'TRIPLE': 0}
    for i in tqdm(range(len(smiles))):
        smile = smiles[i]
        m = Chem.MolFromSmiles(smile)
        b = m.GetBonds()

        for j in range(len(b)):
            bt = b[j].GetBondType().name
            bond_dict[bt] += 1

    return bond_dict

# Number of atoms  with each bond type

def get_bonds_atom(smiles):
    print('Getting bonds')
    bond_dict = {'SINGLE': 0, 'AROMATIC': 0, 'DOUBLE': 0, 'TRIPLE': 0}

    for i in tqdm(range(len(smiles))):
        smile = smiles[i]
        m = Chem.MolFromSmiles(smile)

        bond_type = np.unique(Chem.GetAdjacencyMatrix(m, useBO=True))
        if 1 in bond_type or 1.5 in bond_type:
            bond_dict['SINGLE'] += 1
        if 1.5 in bond_type:
            bond_dict['AROMATIC'] += 1
        if 2 in bond_type or 1.5 in bond_type:
            bond_dict['DOUBLE'] += 1
        if 3 in bond_type:
            bond_dict['TRIPLE'] += 1
    return bond_dict

def get_all_atoms(smiles):
    print('Getting  all atoms')
    atom_dict = {}
    for i in tqdm(range(len(smiles))):
        smile = smiles[i]
        m = Chem.MolFromSmiles(smile)

        atoms = m.GetAtoms()
        a = [atom.GetSymbol() for atom in m.GetAtoms()]

        for atom in a:
            if atom in atom_dict:
                atom_dict[atom] += 1
            else:
                atom_dict[atom] = 1
    return atom_dict

def get_weight(smiles):
    print("getting weights")
    weights = np.zeros(len(smiles))
    for i in tqdm(range(len(smiles))):
        smile = smiles[i]
        m = Chem.MolFromSmiles(smile)
        weight = Descriptors.MolWt(m)
        weights[i] = weight
    return weights

def get_QED(smiles):
    print("Getting QED")
    QEDs = np.zeros(len(smiles))
    for i in tqdm(range(len(smiles))):
        smile = smiles[i]
        m = Chem.MolFromSmiles(smile)
        QEDs[i] = QED.qed(m,w=QED.properties(m))
    return QEDs

def get_logP(smiles):
    print("Getting logP")
    logPs = np.zeros(len(smiles))
    for i in tqdm(range(len(smiles))):
        smile = smiles[i]
        m = Chem.MolFromSmiles(smile)
        logP = Descriptors.MolLogP(m)
        logPs[i] = logP
    return logPs

def get_plogP(smiles):
    print("Getting plogP")
    plogPs = np.zeros(len(smiles))
    for i in tqdm(range(len(smiles))):
        smile = smiles[i]
        m = Chem.MolFromSmiles(smile)
        plogP = penalized_logp(m)
        plogPs[i] = plogP
    return plogPs

def get_SA(smiles):
    print("Get SA")
    SAs = np.zeros(len(smiles))
    for i in tqdm(range(len(smiles))):
        smile = smiles[i]
        m = Chem.MolFromSmiles(smile)
        SA = calculateScore(m)
        SAs[i] = SA
    return SAs


def get_atoms(smiles):
    print('Getting all atoms ')
    atom_dict = {}
    for i in tqdm(range(len(smiles))):
        smile = smiles[i]
        m = Chem.MolFromSmiles(smile)

        atoms = m.GetAtoms()
        a = [atom.GetSymbol() for atom in m.GetAtoms()]
        a = np.unique(a)

        for atom in a:
            if atom in atom_dict:
                atom_dict[atom] += 1
            else:
                atom_dict[atom] = 1

    return atom_dict

data_set = sys.argv[1]

smiles = pickle.load(open(f"{data_set}_smi.pkl","rb"))
#smiles = np.random.choice(smiles,size=100000,replace=False) # Pick 100K samples

statistic = get_weight(smiles)
f = open(f"{data_set}_weight.pkl","wb")
pickle.dump(statistic,f)
f.close()

statistic = get_plogP(smiles)
f = open(f"{data_set}_plogP.pkl","wb")
pickle.dump(statistic,f)
f.close()

statistic  = get_logP(smiles)
f = open(f"{data_set}_logP.pkl","wb")
pickle.dump(statistic,f)
f.close()

statistic = get_SA(smiles)
f = open(f"{data_set}_SA.pkl","wb")
pickle.dump(statistic,f)
f.close()

statistic = get_QED(smiles)
f = open(f"{data_set}_QED.pkl","wb")
pickle.dump(statistic,f)
f.close()

#print(get_all_bonds(smiles))

## Different bond types
## Moses {'SINGLE': 99999, 'AROMATIC': 98361, 'DOUBLE': 88799, 'TRIPLE': 7860}
## Zinc {'SINGLE': 99993, 'AROMATIC': 96945, 'DOUBLE': 88438, 'TRIPLE': 3472}
## QM9 {'SINGLE': 99630, 'AROMATIC': 16449, 'DOUBLE': 47932, 'TRIPLE': 24253}

## All bonds
## Moses {'SINGLE': 1069585, 'AROMATIC': 1105736, 'DOUBLE': 141781, 'TRIPLE': 7924}
## Zinc {'SINGLE': 1349062, 'AROMATIC': 1184764, 'DOUBLE': 179435, 'TRIPLE': 3622}
## QM9 {'SINGLE': 762721, 'AROMATIC': 89404, 'DOUBLE': 60381, 'TRIPLE': 27691}

## Different atomic types
## Moses {'Br': 3247, 'C': 100000, 'N': 99065, 'O': 96432, 'F': 18718, 'Cl': 10499, 'S': 32032}
## Zinc {'C': 100000, 'N': 98121, 'O': 95715, 'S': 38039, 'Br': 4156, 'F': 16941, 'Cl': 14364, 'I': 434, 'P': 79}
## QM9 {'C': 99999, 'N': 61780, 'O': 85045, 'F': 1532}

## All atoms
## Moses {'N': 296305, 'C': 1563499, 'S': 35710, 'O': 224654, 'F': 30618, 'Cl': 11909, 'Br': 3259}
## Zinc {'O': 268922, 'C': 1842636, 'N': 302871, 'S': 44970, 'F': 28457, 'Br': 4313, 'Cl': 16858, 'I': 440, 'P': 81}
## QM9 {'C': 632761, 'O': 140564, 'N': 103773, 'F': 2409}

