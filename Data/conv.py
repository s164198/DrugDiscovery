import pickle as pkl
import pandas as pd

df = pd.read_csv('../ReGraphAF/dataset/moses.csv')

smiles = df.SMILES

# bond_dict
# {'SINGLE': 20761846, 'AROMATIC': 21369673, 'DOUBLE': 2751149, 'TRIPLE': 154734}

with (open("moses_smi.pkl","wb")) as f:
    pkl.dump(smiles,f)