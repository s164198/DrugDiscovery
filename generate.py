import functools
import math
import operator
import os
import pickle
import sys
from argparse import ArgumentParser

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pytorch_lightning as pl
import seaborn as sns
import torch
import torch.utils.data as data
from matplotlib import patches
from rdkit import Chem
from rdkit.Chem import Draw
from rdkit.Chem.Draw import rdMolDraw2D
from torch.utils.data import random_split
from tqdm import tqdm

from dataset import SmilesDataset, MCNNInput
from main import Model, general_args
from model import MolecularCNN, convert_radical_electrons_to_hydrogens


class AcceleratedModelSampler(data.Dataset):
    def __init__(self, model : MolecularCNN, prior, conditioning_names=None, allow_resample=False):
        self.model = model
        self.prior = prior  # Prior to get the properties from.
        self.allow_resample = allow_resample
        self.conditioning_names = conditioning_names  # For use when prior is some tensors and not a dataset.

    def __len__(self):
        return len(self.prior)

    def __getitem__(self, item):
        conditioning = None
        gt = {}
        if self.prior is not None:
            # We have a prior with target property values.
            data = self.prior[item]
            if isinstance(data, MCNNInput):
                conditioning = data.properties
                gt = {name:func(Chem.MolFromSmiles(data.smile)) for name, func in SmilesDataset.properties.items()}
            else:
                conditioning = data
                for idx, name in enumerate(sorted(self.conditioning_names)):
                    gt[name] = data[idx]

        smile, bad, final_mol, _, _, _, _, resamplings =\
            self.model.cpu_sample(max_nodes=self.model.max_num_nodes,
                                   temperature=1.0,
                                   ensure_connected=True,
                                   properties=conditioning[None],
                                   allow_resample=self.allow_resample)

        # Return all the calculated properties for the produced mol.
        properties = {name:func(final_mol) for name, func in SmilesDataset.properties.items()}

        return smile, bad, resamplings, properties, gt

def collect_samples(model : Model, prior, sample_save_path=''):
    sampler = AcceleratedModelSampler(model.model, prior, model.hparams.conditioning, allow_resample=False)

    results = {name: [] for name in ['smiles', 'bad', 'resamplings', 'properties', 'conditioning']}

    f = sys.stdout
    if sample_save_path:
        f = open(sample_save_path, 'w')

    for result_batch in tqdm(data.DataLoader(sampler, batch_size=8,
                                             num_workers=model.hparams.num_workers)):
        smile, bad, resamplings, properties, conditioning = result_batch
        results['smiles'].append(smile)
        results['bad'].append(bad)
        results['resamplings'].append(resamplings)
        results['properties'].append(properties)
        results['conditioning'].append(conditioning)
        good_smile = [smi for idx, smi in enumerate(smile) if not bad[idx]]
        print(*good_smile, sep='\n', file=f)

    # Now that we have the results we want to 'flatten' the datastructures.
    def flatten_structure(values):
        # Smelly code I know.
        if isinstance(values[0], list) or isinstance(values[0], tuple):
            return functools.reduce(operator.iconcat, values)
        elif isinstance(values, list) and isinstance(values[0], torch.Tensor):
            if not values[0].shape:
                return torch.stack(values, 0)
            return torch.cat(values, 0)
        elif isinstance(values[0], dict):
            flattened = {name: [] for name in values[0].keys()}
            for entries in values:
                for name, values in entries.items():
                    flattened[name] += values
            # Now we have turned the list of dicts into a dict of lists.
            # We want to then concatenate flatten these lists.
            for name in flattened.keys():
                flattened[name] = flatten_structure(flattened[name])
            return flattened
        else:
            raise NotImplementedError(type(values[0]))

    results = {name: flatten_structure(value) for name, value in results.items()}

    # Make it into a dataframe now, so it's more manageable.
    calculated_properties = results.pop('properties')
    for name, value in calculated_properties.items():
        results[name] = value

    conditioned_properties = results.pop('conditioning')
    for name, value in conditioned_properties.items():
        results['cond_' + name] = value

    return results

def draw_mol(mol, size=(250, 200), legendsize=None, linewidth=1, indices=False, kekulize=False, legend='', svg=True):
    # MolDraw2DSVG, MolDraw2DCairo,
    if svg:
        d = rdMolDraw2D.MolDraw2DSVG(*size)  # or MolDraw2DSVG to get SVGs
    else:
        d = rdMolDraw2D.MolDraw2DCairo(*size)
    d.drawOptions().addStereoAnnotation = True
    d.drawOptions().addAtomIndices = indices
    if legendsize is not None:
        d.drawOptions().legendFontSize = legendsize
    d.SetLineWidth(linewidth)  # Maybe not?
    d.SetFontSize(0.8)  # Maybe not?
    mol = rdMolDraw2D.PrepareMolForDrawing(mol, kekulize=kekulize)
    d.DrawMolecule(mol, legend=legend)
    d.FinishDrawing()
    return d

def draw_molecules(results, dataset, plot_save_path):
    smiles = results['smiles'][:9]
    molstodraw = [rdMolDraw2D.PrepareMolForDrawing(Chem.MolFromSmiles(smile), kekulize=False) for smile in smiles]
    mol_drawings = Draw.MolsToGridImage(molstodraw, legends=smiles, useSVG=True,
                                        subImgSize=(330, 330), molsPerRow=3)
    print(mol_drawings, file=open(os.path.join(plot_save_path, 'example_generated_montage.svg'), 'w+'))
    #print(mol_drawings, file=open(os.path.join(plot_save_path, 'example_MOSES_montage.svg'), 'w+')) # TOD0?

    # example_mol = Chem.MolFromSmiles(results['smiles'][0])
    example_mol = Chem.MolFromSmiles(dataset.smiles[2020])

    # model.model.sample_cpu(debug=True)
    #with open(os.path.join(plot_save_path, 'example_mol.svg'), 'wb') as f:
    #    f.write(draw_mol(Chem.MolFromSmiles(example_mol)).GetDrawingText())
    print(draw_mol(example_mol).GetDrawingText(), file=open(os.path.join(plot_save_path, 'example_mol.svg'), 'w+'))

    node_type, adj_type = dataset.encode_mol(SmilesDataset.process_mol(example_mol))
    ordered_mol = dataset.get_validity_masks(node_type, adj_type, return_mol=True)
    ordered_mol = convert_radical_electrons_to_hydrogens(ordered_mol)
    Chem.SanitizeMol(ordered_mol)
    print(draw_mol(ordered_mol, indices=True, kekulize=True).GetDrawingText(), file=open(os.path.join(plot_save_path, 'example_mol_processed.svg'), 'w+'))


def sample_conditioned(model:Model, K, sample_save_path='', plot_save_path='', test_prior=False):
    log_file = open(os.path.join(plot_save_path, 'sample_log' + str(K) + '.txt'), 'a')
    with open(os.path.join('dataset', 'MOSES_dataset_v1_train_unique.pickle'), 'rb') as f:
        train_smiles = pickle.load(f)

    if not test_prior:
        # Sampling based on a single conditioning. # TODO: Should probably be the input.
        prior, _ = random_split(model._train_dataset, [K, len(model._train_dataset) - K])
    else:
        prior, _ = random_split(model._test_dataset, [K, len(model._test_dataset) - K])

    results = collect_samples(model, prior, sample_save_path=sample_save_path)


    df = pd.DataFrame.from_dict(results)
    df['valid'] = ~df['bad']

    ## VALIDITY, NOVELTY, UNIQUENESS
    df['novel'] = ~df['smiles'].map(train_smiles.__contains__)
    print('Done sampling.')
    print('Average number of resamplings:', df['resamplings'].mean())
    print('Validity', df['valid'].mean())  # Fraction of valid.
    print('Validity', df['valid'].mean(), file=log_file)

    # For the rest of statistics, we only want the valid molecules.
    df_valid = df[df['valid']]
    print('Novelty', df_valid['novel'].mean())  # Fraction of valid that are novel. # FIXME: Change to match MOSES calculation
    print('Uniqueness', df_valid['smiles'].nunique() / len(df_valid))  # Fraction of valid molecules that are unique.
    print('Average number of resamplings in valid molecules:', df_valid['resamplings'].mean())

    print('Novelty', df_valid['novel'].mean(), file=log_file)
    print('Uniqueness', df_valid['smiles'].nunique() / len(df_valid), file=log_file)

    ## PLOT SOME MOLECULES
    draw_molecules(results, SmilesDataset(model.hparams.dataset_path,
                            conditioning=model.hparams.conditioning,
                            validity_masks=not model.hparams.no_validity_masks and
                                           not model.hparams.validity_masks_from_model), plot_save_path)

    ## DISTRIBUTION PLOTS
    f, axes = plt.subplots(min(len(SmilesDataset.properties), 2),
                           (len(SmilesDataset.properties) + 1) // 2,
                           figsize=(3.5 * (len(SmilesDataset.properties) + 1) // 2, 7))
    axes = axes.flatten()
    for ndx, name in enumerate(sorted(SmilesDataset.properties.keys())):
        ax = axes[ndx]
        sns.distplot(df['cond_' + name],
                     hist=False, kde=True, color="r", ax=ax)
        sns.distplot(df_valid[name], kde=True, color="b", ax=ax)
        # ax.get_legend().remove()
    legends = [(plt.Line2D((0, 1), (0, 0), color="b"), 'Generated'),
               (plt.Line2D((0, 1), (0, 0), color="r"), 'MOSES')]
    f.legend(*zip(*legends), loc='lower center', ncol=len(legends))
    # plt.tight_layout()
    plt.subplots_adjust(bottom=0.12)  # Make space for legend.
    plt.savefig(os.path.join(plot_save_path, "figure_1.pdf"), format="pdf", dpi=1000)
    plt.show()

    ## SUBSET DISTRIBUTION PLOTS
    graphAF = pickle.load(open(os.path.join('Data', 'graphAFstat.pickle'), 'rb'))
    f, axes = plt.subplots(2, 2, figsize=(7, 7))
    axes = axes.flatten()
    for ndx, name in enumerate(['logp', 'SA', 'molw', 'qed']):
        ax = axes[ndx]
        if name in graphAF:
            sns.distplot(graphAF[name],
                         hist=False, kde=True, color="yellow", ax=ax)
        sns.distplot(df['cond_' + name],
                     hist=False, kde=True, color="r", ax=ax)
        sns.distplot(df_valid[name], kde=True, color="b", ax=ax)
        ax.set_xlabel(name, fontsize=14)
        # ax.get_legend().remove()
    legends = [(plt.Line2D((0, 1), (0, 0), color="b"), 'MolecularCNN'),
               (plt.Line2D((0, 1), (0, 0), color="yellow"), 'GraphAF'),
               (plt.Line2D((0, 1), (0, 0), color="r"), 'MOSES')]
    f.legend(*zip(*legends), loc='lower center', ncol=len(legends), prop={'size': 12})
    plt.tight_layout()
    plt.subplots_adjust(bottom=0.14)  # Make space for legend.
    plt.savefig(os.path.join(plot_save_path, "figure_1_subset.pdf"), format="pdf", dpi=1000)
    plt.show()

    # Polynomial Regression
    def polyfit(x, y, degree=1):
        # https://stackoverflow.com/questions/893657/how-do-i-calculate-r-squared-using-python-and-numpy
        results = {}

        coeffs = np.polyfit(x, y, degree)

        # Polynomial Coefficients
        results['polynomial'] = coeffs.tolist()

        # r-squared
        p = np.poly1d(coeffs)
        # fit values, and mean
        yhat = p(x)  # or [p(z) for z in x]
        ybar = np.sum(y) / len(y)  # or sum(y)/len(y)
        ssreg = np.sum((yhat - ybar) ** 2)  # or sum([ (yihat - ybar)**2 for yihat in yhat])
        sstot = np.sum((y - ybar) ** 2)  # or sum([ (yi - ybar)**2 for yi in y])
        results['determination'] = ssreg / sstot

        return results

    red = '#990000'
    orange = '#FC7634'
    ## PROPERTY CONDITIONING PLOTS
    # https://stackoverflow.com/questions/27164114/show-confidence-limits-and-prediction-limits-in-scatter-plot
    # f, axes = plt.subplots(2, len(model.hparams.conditioning) // 2, figsize=(7, 7))
    for ndx, cond in enumerate(sorted(model.hparams.conditioning)):
        fit = polyfit(df_valid['cond_' + cond], df_valid[cond])
        # Make regression plot.
        g = sns.jointplot(df_valid['cond_' + cond], df_valid[cond], kind='kde', n_levels=4, shade_lowest=False)
        g.plot_joint(sns.regplot, scatter=True, marker='.', ci=95,
                     scatter_kws={'s': 4, 'color': orange, 'alpha':0.5}, line_kws={'color': red})
        g.plot_joint(sns.kdeplot, n_levels=4, linewidths=1)
        x0 = df['cond_' + cond].min()
        x1 = df['cond_' + cond].max()
        plt.plot([x0, x1], [x0, x1], color='k', linestyle='--', label='target', zorder=1)
        plt.suptitle('Regression', x=0.15, size=20)
        plt.ylabel('Generated ' + cond)
        plt.xlabel('Conditioned ' + cond)

        plt.legend(['Regression line',
                    'Reference y=x',
                    f'$R^2 ={fit["determination"]:.2f}$'], loc='upper left')
        plt.savefig(os.path.join(plot_save_path, "figure_2" + cond + ".pdf"), format="pdf", dpi=1000)
        plt.show()
        print('Fit for ' + cond, 'with', len(df_valid), 'observations is', fit)
        print('Fit for ' + cond, 'with', len(df_valid), 'observations is', fit, file=log_file)
        sign = '' if fit["polynomial"][1] < 0 else '+'
        print(f'Regression line for {cond} is $y={fit["polynomial"][0]:.2f}x{sign}{fit["polynomial"][1]:.2f}')
        print(f'Regression line for {cond} is $y={fit["polynomial"][0]:.2f}x{sign}{fit["polynomial"][1]:.2f}', file=log_file)
        # Make residual plot.
        sns.jointplot(df_valid['cond_' + cond], df_valid[cond], kind='resid', line_kws={'color': red, 'linestyle': '-'},
                      scatter_kws={'s': 4, 'color': orange})
        plt.suptitle('Residuals', x=0.15, size=20)
        # plt.ylabel('Conditioned - Generated ' + cond)
        plt.ylabel('Residuals for fit on ' + cond)
        plt.xlabel('Conditioned ' + cond)
        plt.legend(['Regression line'])
        # plt.legend([f'Regression line $y={fit["polynomial"][0]:.2f}x{sign}{fit["polynomial"][1]:.2f}$', 'Residuals', f'$R^2 ={fit["determination"]:.2f}$'], loc='upper left')
        plt.savefig(os.path.join(plot_save_path, "figure_3" + cond + ".pdf"), format="pdf", dpi=1000)
        plt.show()


    if model.hparams.conditioning:
        # sorted(model.hparams.conditioning)
        resolution = min(math.ceil( K**(1/len(model.hparams.conditioning)) ), 64)  # Not too many.
        # x = df[['cond_' + cond for cond in sorted(model.hparams.conditioning)]]
        ma, mi = df.max(), df.min()
        xs = []
        for cond in sorted(model.hparams.conditioning):
            span = ma['cond_' + cond] - mi['cond_' + cond]
            xs.append(np.linspace(mi['cond_' + cond] - span * 1.5,
                                  ma['cond_' + cond] + span * 1.5,
                                  resolution))

        datapoints = np.stack(np.meshgrid(*xs), -1)  # (res, res, res, 3) if there are 3 conds.
        datapoints = torch.tensor(datapoints.reshape(-1, len(xs)), dtype=torch.float)

        df_reg = pd.DataFrame.from_dict(collect_samples(model, datapoints))
        df_reg['valid'] = ~df_reg['bad']
        # For the rest of statistics, we only want the valid molecules.
        df_reg_valid = df_reg[df_reg['valid']]

        for cond in sorted(model.hparams.conditioning):
            sns.jointplot(df_reg_valid['cond_' + cond], df_reg_valid[cond],
                          kind='reg', marker='.', fit_reg=False,
                          scatter_kws={'s': 9, 'color': orange},
                          line_kws={'color': red, 'label': 'Regression line'})

            sns.rugplot(df_reg['cond_' + cond][df_reg['bad']], axis='x', capstyle='round', color='red',
                        label='Invalid molecule')

            # sns.scatterplot(df_reg_valid['cond_' + cond], df_reg_valid[cond], kind='kde', n_levels=4)
            # x0 = mi['cond_' + cond]
            # x1 = ma['cond_' + cond]
            x0, x1 = plt.xlim()
            plt.plot([x0, x1], [x0, x1], color='k', linestyle='--', label='Reference y=x', zorder=1)
            plt.vlines([mi['cond_' + cond], ma['cond_' + cond]], mi['cond_' + cond], ma['cond_' + cond],
                       label='Data bounds')
            plt.suptitle('Induction', x=0.15, size=20)
            plt.ylabel('Generated ' + cond)
            plt.xlabel('Conditioned ' + cond)
            plt.legend(loc='upper left')
            plt.tight_layout()
            # ['Regression line',
            #             'Reference y=x',
            #             'Test']
            plt.savefig(os.path.join(plot_save_path, "figure_4" + cond + ".pdf"), format="pdf", dpi=1000)
            plt.show()

        if len(model.hparams.conditioning) == 2:
            fig, ax = plt.subplots(1)
            y = df_reg_valid[['cond_' + cond for cond in sorted(model.hparams.conditioning)]]
            yhat = df_reg_valid[[cond for cond in sorted(model.hparams.conditioning)]]
            errors = yhat - y.values
            std_errors = (errors - errors.mean(0)) / errors.std(0)
            std_dists = np.sqrt((std_errors ** 2).sum(1))
            xy = y.values
            z = std_dists.values
            # cmap = plt.get_cmap('inferno')
            # norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
            # levels = MaxNLocator(nbins=15).tick_values(z.min(), z.max())
            # plt.contourf(xy[..., 1].flatten(), xy[..., 0].flatten(), z.flatten())
            plt.tricontourf(xy[..., 0].flatten(), xy[..., 1].flatten(), z.flatten(), antialiased=True)
            cbar = plt.colorbar()
            cbar.ax.get_yaxis().labelpad = 15
            cbar.ax.set_ylabel('Standardized error', rotation=270)
            plt.plot(xy[..., 0], xy[..., 1], marker='.', markersize=1, linestyle='None', color=orange)
            names = sorted(model.hparams.conditioning)
            plt.xlabel('Conditioned ' + names[0])
            plt.ylabel('Conditioned ' + names[1])
            rect = patches.Rectangle((mi['cond_' + names[0]], mi['cond_' + names[1]]),
                                     ma['cond_' + names[0]] - mi['cond_' + names[0]],
                                     ma['cond_' + names[1]] - mi['cond_' + names[1]], linewidth=1, edgecolor='k',
                                     facecolor='none')
            ax.add_patch(rect)
            plt.tight_layout()
            plt.savefig(os.path.join(plot_save_path, "figure_4.pdf"), format="pdf", dpi=1000)
            plt.show()


    # prior = torch.linspace(min_val, max_val, steps, dtype=torch.float)[:, None].repeat([100, 1])
    return results['smiles']


    # plt.savefig('plot.png')

    # print(df, df['bad'].mean())


if __name__ == "__main__":
    parser = ArgumentParser()
    # parser = pl.Trainer.add_argparse_args(parser)  # Add trainer options.

    # General training parameters
    #parser.add_argument('--seed', default=42, type=int, help='Set the seed for the RNG.')
    #parser.add_argument('--num_workers', default=0, type=int, help='Number of workers in dataloader.')
    #parser.add_argument('--log_level', default=0, type=int,
    #                    help='The level of debugging logs to save (mainly tensorboard logs)')
    # parser.add_argument('--dataset_path', default='ReGraphAF/dataset/250k_rndm_zinc_drugs_clean_sorted.smi')

    parser = general_args()
    parser.add_argument('--num', default=1000, type=int)
    parser.add_argument('--load_path', default='final_models/logpmolwconditioning/')
    parser.add_argument('--save_sample_path', default='')

    # Hyperparameters
    parser = Model.add_model_specific_args(parser)
    parser = pl.Trainer.add_argparse_args(parser)  # Add trainer options.
    args = parser.parse_args()
    if args.load_path:
        args.tags_csv = os.path.join(args.load_path, 'meta_tags.csv')
        latest_ckpt_name = os.listdir(os.path.join(args.load_path, 'checkpoints'))[0]  # Assume there's just one.
        args.model_path = os.path.join(args.load_path, 'checkpoints', latest_ckpt_name)
        if not args.save_sample_path:
            args.save_sample_path = os.path.join(args.load_path, f'sample{args.num}.smi')

    test_prior = False
    if args.test:
        test_prior = True

    model = Model.load_from_checkpoint(checkpoint_path=args.model_path,
                                       tags_csv=args.tags_csv,
                                       map_location='cpu')

    if args.num_workers is not None:
        model.hparams.num_workers = args.num_workers
    # vars(model.hparams).update(vars(args))  # Overwrite with given arguments.
    model.eval()
    model.freeze()
    molecules = sample_conditioned(model, args.num, sample_save_path=args.save_sample_path, plot_save_path=args.load_path, test_prior=test_prior)
    exit()
    #if args.gpus:
    #     model = model.to('cuda:' + str(args.gpus[0]))

    # uniqueness, validity, novelty, molecules, tiny_molecules = model.sample(num=args.num)
    if False:
        from main_RL import steric_strain_filter

        print('Generating', args.num, 'molecules')
        print('Steric strain test, bad molecule (not added to result), %steric test success, %good molecules, num resamplings, smiles')
        molecules = []
        invalid = 0
        k = 0
        bar = tqdm(total=args.num)
        accum_resamplings = 0
        while len(molecules) < args.num:
            smile, bad, final_mol, node_type, edge_type, log_probs_node, log_probs_edge, resamplings \
                = model.model.cpu_sample(max_nodes=model.model.max_num_nodes, allow_resample=False)
            if not bad:
                molecules.append(smile)
                k+= steric_strain_filter(Chem.MolFromSmiles(smile))
                bar.update(1)
            accum_resamplings += resamplings
            invalid += bad
            bar.write(f'{int(steric_strain_filter(Chem.MolFromSmiles(smile)))} {int(bad)} '
                      f'{k/len(molecules):.3f} {(len(molecules)/(len(molecules)+invalid)):.3f} '
                      f'{resamplings} {smile}  ')

        print('Resampling happens an average of', accum_resamplings/(len(molecules)+invalid), 'times')
        if args.save_sample_path:
            with open(args.save_sample_path, 'w') as f:
                print(*molecules, sep='\n', file=f)
    elif True:
        # Needs a random subset of the training set for properties.

        # molecules, _ = model.model.batch_sample(args.num, max_nodes=48, temperature=0.5,
        #                                         ensure_connected=True, properties=properties)
        print('Generating', args.num, 'molecules')
        with torch.no_grad():
            molecules, *_ = model.sample(args.num)

        if args.save_sample_path:
            with open(args.save_sample_path, 'w') as f:
                print(*molecules, sep='\n', file=f)
        else:
            print('Generation complete. Since no save_sample_path was given, we will now print the result.')
            for molecule in molecules:
                print(molecule)
    else:
        # Some property optimization
        goal = 3
        top = []
        new = [None]
        while new:
            new.clear()
            span = range(250, 350)
            properties = torch.tensor([(weight, goal) for weight in span] * 10, dtype=torch.float)
            molecules, _, _ = model.model.batch_sample(len(properties), max_nodes=48, temperature=0.5,
                                                    ensure_connected=True, properties=properties)
            print('batch done, generated', len(molecules), 'molecules')
            for smile in molecules:
                mol = Chem.MolFromSmiles(smile)
                wt = Descriptors.HeavyAtomMolWt(mol)
                logP =  Descriptors.MolLogP(mol)
                if logP > goal:
                    new.append((logP, wt, smile))
                    goal = logP
                    print(goal)
            top = top + new
            for logP, wt, smile in sorted(new):
                print(smile, f'with logP={logP:.3f}, wt={wt:.3f}')

    #print(uniqueness, validity, novelty, 'for', len(molecules), '/', len(molecules)+len(tiny_molecules))
    #for smile in molecules:
    #    print(smile)

    # trainer = pl.Trainer.from_argparse_args(args)
    # trainer.fit(model)