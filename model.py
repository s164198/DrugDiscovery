import copy
import math
from typing import Union

from rdkit import Chem
import rdkit.Chem.Descriptors
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch_geometric as geom
from rdkit.Chem import Draw
from torch.distributions.utils import logits_to_probs

# Weakness 1:
#  Since the GCNConv is permutation-invariant, there is no way to represent or learn Stereoisomerism.
#  This could be fixed by letting a separate network learn this on the final molecule. But it shows that one should not
#    e.g. expect to regress atomic positions well from a basic GNN. Note how a SMILES-representation lack this weakness.
# We also notice while training with Zink250, that this dataset contains ions/charges, which our model does not support.

class GraphAFConv(nn.Module):
    def __init__(self, in_channels, out_channels, num_relations, bias=True):
        super().__init__()
        self.num_relations = num_relations
        self.weight = nn.Parameter(torch.Tensor(self.num_relations, in_channels, out_channels))

        self.lin = torch.nn.Linear(in_channels, out_channels, bias=bias)

        with torch.no_grad():
            nn.init.zeros_(self.lin.bias)
            bound = 1.0 / math.sqrt(in_channels) / self.num_relations
            self.weight.data.uniform_(-bound, bound)

    def forward(self, x, adj_feat):
        message = torch.matmul(torch.matmul(adj_feat, x[..., None, :, :]), self.weight)
        result = message.sum(-3) + self.lin(x)  # relational messages + self
        return result

class RGCN(nn.Module):
    def __init__(self, in_channels, num_relations=3, out_channels=128, hidden_size=128):
        super().__init__()
        # Interesting paper about the power of graph conv types: https://arxiv.org/pdf/1810.00826.pdf
        # The difference is transductive/spectral/symmetric vs inductive/spatial/mean.

        self.layers = nn.ModuleList([GraphAFConv(in_channels, hidden_size, num_relations, bias=True), nn.ReLU(),
                                     geom.nn.DenseGraphConv(hidden_size, hidden_size), nn.ReLU(),
                                     geom.nn.DenseGraphConv(hidden_size, hidden_size), nn.ReLU(),
                                     geom.nn.DenseGraphConv(hidden_size, hidden_size), nn.ReLU(),
                                     geom.nn.DenseGraphConv(hidden_size, out_channels)])

        with torch.no_grad():
            for layer in self.layers:
                if isinstance(layer, geom.nn.DenseGraphConv):
                    nn.init.zeros_(layer.lin.bias)


    def forward(self, node_feat, adj_feat):
        adj = torch.sum(adj_feat, dim=-3)  # Adjacency matrix.
        x = self.layers[1](self.layers[0](node_feat, adj_feat))  # Relational GraphConv
        x = self.layers[3](self.layers[2](x, adj)) + x  # Graphconv + residual connection
        x = self.layers[5](self.layers[4](x, adj)) + x  # Graphconv + residual connection
        return self.layers[-1](x, adj)


class PredictionNetwork(nn.Module):
    def __init__(self, in_features, out_features, hidden_size, batch_normalization=False, dropout=0.):
        super().__init__()
        self.pre_bn = nn.Linear(in_features, hidden_size, bias=not batch_normalization)
        self.bn = None
        if batch_normalization:
            # Just a lower momentum since we probably have many correlated features in a batch.
            # E.g. 34 molecules are turned into more than 32*100 generation steps, but all these steps have the exact
            # same value for the properties.
            self.bn = nn.BatchNorm1d(hidden_size, momentum=0.01)
        self.post_bn = nn.Sequential(nn.ReLU(), nn.Linear(hidden_size, hidden_size),
                                     nn.ReLU(), nn.Linear(hidden_size, out_features))
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        x = self.pre_bn(x)
        if self.bn is not None:
            x = self.bn(x.reshape(-1, x.shape[-1])).reshape_as(x)  # We flatten over the graph generation steps.
        x = self.dropout(x.reshape(-1, x.shape[-1])).reshape_as(x)
        return self.post_bn(x)


class MolecularCNN(nn.Module):
    def __init__(self, node_types, edge_types, max_num_nodes, max_graph_bandwidth, num_properties=0, hidden_size=128,
                 dropout=0., batchnorm=True):
        super().__init__()
        self.criteria = nn.CrossEntropyLoss(reduction='none')
        self.node_types = node_types
        self.edge_types = edge_types
        self.max_num_nodes = max_num_nodes
        self.max_graph_bandwidth = max_graph_bandwidth  # The graph bandwidth that this model was trained width.
        self.num_properties = num_properties

        ### SET UP MASKS
        self.setup_masks(max_num_nodes)

        ### SET UP NETWORKS
        self.rgcn = RGCN(len(self.node_types), len(self.edge_types) - 1, # NO_BOND is not a real relation.
                         out_channels=hidden_size, hidden_size=256)

        ## Node property conditioning.
        node_net_out_size = len(self.node_types)
        self.property_node_net = None
        if 0 < num_properties:
            # If we have properties, we need to condition the nodes on these.
            join_size = hidden_size  # Could also be smaller/grater.
            self.property_node_net = PredictionNetwork(join_size + num_properties,
                                                       len(self.node_types), hidden_size, batchnorm, dropout)
            node_net_out_size = join_size
        self.node_net = nn.Sequential(nn.Linear(hidden_size, hidden_size), nn.ReLU(),
                                      nn.Linear(hidden_size, hidden_size), nn.ReLU(),
                                      nn.Linear(hidden_size, node_net_out_size))
        ## Edge property conditioning.
        self.edge_net = nn.Sequential(nn.Linear(hidden_size * 3, hidden_size), nn.ReLU(),
                                      nn.Dropout(dropout),
                                      nn.Linear(hidden_size, hidden_size), nn.ReLU(),
                                      nn.Linear(hidden_size, hidden_size))
        self.property_edge_net = PredictionNetwork(hidden_size + num_properties + max_graph_bandwidth,
                                                   len(self.edge_types), hidden_size, batchnorm, dropout)

        # VVV ONLY FOR EXPERIMENTATION
        self.graph_relevancy = nn.Dropout(p=0.0)  # Don't drop out graph features.
        self.property_relevancy = nn.Dropout(p=0.0)  # Don't zero out property vectors.
        self.edge_step_relevancy = nn.Dropout(p=0.0)  # Don't drop out the prediction step. (a kind of property vector.)
        # Dropout adds noise to the specific features, such that the model must rely less on it.
        # For example we tried training with a graph relevancy going from 1 to 0 slowly, so it learns to use properties.

    def setup_masks(self, max_num_nodes):
        for name, mask in zip(['node_masks', 'edge_masks', 'edge_pred_ji', 'target_edge_masks', 'edge_pred_band'],
                              initialize_masks(max_num_nodes, self.max_graph_bandwidth)):
            self.register_buffer(name, mask)
        graph_sizes = self.node_masks.sum(-1)  # The graph size for a step is the number of nodes.
        graph_sizes[0] = 1  # We don't want to divide by zero.
        self.register_buffer('masked_node_normalization', graph_sizes[:max_num_nodes])
        self.register_buffer('masked_edge_normalization', graph_sizes[max_num_nodes:])

    def get_validity_masks(self, node_categorical, edge_categorical, edge_pred_ji, one_step_prediction):
        device = node_categorical.device
        node_categorical = node_categorical.cpu()  # This will be faster on CPU, as its a sequential algorithm.
        edge_categorical = edge_categorical.cpu()

        valid_edge_mask = torch.ones((node_categorical.shape[0], len(edge_pred_ji), len(self.edge_types)),
                                      dtype=torch.bool)  # (batch, repeat-N, B)

        for k in range(node_categorical.shape[0]): # For every molecule.
            edge_step = 0
            rw_mol = Chem.RWMol()
            for i in range(node_categorical.shape[1]): # For every atom.
                # We could make a node mask here. E.g. put zero probability for END_GRAPH for the empty graph.
                atom_type = self.node_types[node_categorical[k, i].item()]

                if atom_type:
                    rw_mol.AddAtom(Chem.Atom(atom_type))
                else:
                    break  # Done with this molecule.

                edge_total = min(i, self.max_graph_bandwidth)  # The number of edges to sample.
                start = max(0, i - self.max_graph_bandwidth)
                for j in range(start, start + edge_total):
                    if not one_step_prediction:
                        # Now to make the valid edge prediction mask.
                        for b, edge_type in enumerate(self.edge_types[1:], start=1):
                            # If this is an actual bond. (NO_BOND is always valid)
                            rw_mol.AddBond(i, j, edge_type)
                            valid_edge_mask[k, edge_step, b] = check_valency(rw_mol)
                            rw_mol.RemoveBond(i, j)
                    edge_type = self.edge_types[edge_categorical[k, i, j].item()]
                    if edge_type:
                        rw_mol.AddBond(i, j, edge_type)
                    # The input molecule should be valid. Let's do a sanity check.
                    # assert valid_edge_mask[k, edge_step, edge_categorical[k, i, j].item()]
                    edge_step += 1
            # Done building molecule.

            # assert check_valency(rw_mol)  # Gave issues with Zinc250, which turned out to contain ions/charged atoms.

            # In case we are doing one-step-predictions, we can first create the mask after the molecule is built.
            if one_step_prediction:
                for edge_step, (j, i) in enumerate(edge_pred_ji.cpu()):
                    i, j = i.item(), j.item()
                    # Now to make the valid edge prediction mask.
                    for b, edge_type in enumerate(self.edge_types[1:], start=1):
                        # If this is an actual bond. (NO_BOND is always valid)
                        rw_mol.AddBond(i, j, edge_type)
                        valid_edge_mask[k, edge_step, b] = check_valency(rw_mol)
                        rw_mol.RemoveBond(i, j)

        return valid_edge_mask.to(device=device, non_blocking=True)

    def embed_graph(self, node_feat, edge_feat, node_masks=None):
        ### GET GRAPH EMBEDDINGS
        def flatten(x):
            return x.reshape(-1, *x.shape[2:])

        # Transform using Relational Graph Neural Network.
        node_emb = self.rgcn(flatten(node_feat), flatten(edge_feat))  # (batch*repeat, N, H)
        node_emb = node_emb.reshape(*node_feat.shape[:-1], -1)  # (batch, repeat, N, H)

        # If there's any bias in the layers of the R-GCN, we have to re-mask the outputs.
        if node_masks is not None:
            node_emb = torch.where(node_masks, node_emb, node_emb.new_zeros(1))  # (batch, repeat, N, H)

        # Calculate the graph embedding. We choose sum, as it's sufficiently powerful.
        # Note the magnitude of the features from the empty graph to a full graph varies widely.
        graph_emb = torch.sum(node_emb, -2)  # (batch, repeat, H)

        return graph_emb, node_emb

    def predict_node(self, node_pred_features, properties, node_normalization):
        # We must normalize the output to bring the features on the same scale. (Otherwise NaN/Inf will follow)
        node_logits = self.node_net(node_pred_features) / node_normalization[None, :, None]  # (batch, N, K)

        if self.property_node_net is not None:
            assert properties is not None, "Network was trained with properties. It needs to be given these!"
            properties = properties[:, None, :].expand(node_logits.shape[0], node_logits.shape[1], -1)

            node_pred_features = self.graph_relevancy(node_logits) # Applies some "noise",
            properties = self.property_relevancy(properties)  # - such that network must rely on the other feature more.

            # We have a property conditioning network. This means the output of node_net was simply graph features.
            node_pred_features = torch.cat([node_pred_features, properties], -1) # (batch, N, H+P)
            node_logits = self.property_node_net(node_pred_features)  # (batch, N, B)

        return node_logits

    def predict_edge(self, edge_pred_features, properties, edge_ji, edge_normalization, edge_validity_masks=None):
        edge_logits = self.edge_net(edge_pred_features) / edge_normalization[None, :, None]  # (batch, repeat-N, B)

        if self.property_edge_net is not None:
            edge_step = F.one_hot((edge_ji[..., 1] - edge_ji[..., 0] - 1), self.max_graph_bandwidth).float()
            edge_step = edge_step[None, :].expand(edge_logits.shape[0], edge_logits.shape[1], -1)

            edge_pred_features = self.graph_relevancy(edge_logits)

            edge_step = self.edge_step_relevancy(edge_step)

            conditioning = [edge_pred_features, edge_step]
            if self.num_properties > 0:
                assert properties is not None, "Network was trained with properties. It needs to be given these!"
                properties = properties[:, None, :].expand(edge_logits.shape[0], edge_logits.shape[1], -1)
                properties = self.property_relevancy(properties)
                conditioning.append(properties)
            # We have a property conditioning network. Give it normalized edge predictive features.
            edge_pred_features = torch.cat(conditioning, -1)
            edge_logits = self.property_edge_net(edge_pred_features)  # (batch, repeat-N, B)

        if edge_validity_masks is not None:
            # We replace invalid edge type log probs with -infinity, corresponding to log(p) for p = 0+.
            edge_logits = torch.where(edge_validity_masks,
                                      edge_logits, edge_logits.new_full((1,), fill_value=float('-inf')))

        return edge_logits

    def categorical_to_one_hot(self, node_feat, edge_feat):
        if node_feat.dtype == torch.long:
            node_feat = F.one_hot(node_feat, num_classes=len(self.node_types)).float()  # (batch, N, K) for K types.

        if edge_feat.dtype == torch.long:
            edge_feat = F.one_hot(edge_feat, num_classes=len(self.edge_types))  # (batch, N, N, 4)
            # We turn the one-hot into channels, so it's suitable for convolutions. We also drop the NO_BOND part.
            edge_feat = edge_feat[..., 1:].transpose(-3, -1).float()  # (batch, 3, N, N)
        return node_feat, edge_feat

    def apply_full_prediction_masks(self, node_feat, edge_feat):
        # APPLY FULL-GRAPH PREDICTION MASKS.
        # We expand the masks to match the number of graphs in the batch.
        node_masks = self.node_masks[None, :, :, None].expand(node_feat.shape[0], -1, -1, 1)  # (batch, repeat, N, K/H)
        edge_masks = self.edge_masks[None, :, None, :, :].expand(edge_feat.shape[0], -1,  # (batch, repeat, B, N, N)
                                                                 edge_feat.shape[1], -1, -1)

        # Apply masks. This adds a masked feature set for every one-step prediction making out the molecule.
        node_feat = torch.where(node_masks, node_feat, node_feat.new_zeros(1))  # (batch, repeat, N, K)
        edge_feat = torch.where(edge_masks, edge_feat, edge_feat.new_zeros(1))  # (batch, repeat, B, N, N)
        return node_feat, edge_feat, node_masks

    def forward(self, node_features, edge_features, properties=None, mode='both', edge_ji=None,
                one_step_prediction=False, validity_masks: Union[bool, torch.Tensor]=True):
        """
        Features can be given as categorical LongTensors. This means a size (batch, N) tensor for the node_features,
        or a size (batch, N, N) for the edge_features. It should be that N < self.max_graph_nodes, if masks are used.
        :param node_features: Graph features. Must be FloatTensor of (batch, N, num_feat)
        :param edge_features: Adjacency type matrix of shape (batch, B, N, N)
        :param properties: Some properties of the graphs given, for condition generation.
        :param mode: The mode must be either 'node', 'edge' or 'both'. It specifies which logits to calculate.
        :param one_step_prediction:
          False : Full graph prediction. Given a batch of complete graphs, use the masks to expand each graph into
                  a set of one-step predictions. The logits for each step are returned.
          True: One-step prediction. A one-step prediction for a node or edge, usually not both.
        :param validity_masks: If we should mask out invalid edge generation steps using chemical knowledge.
        :return node_logits: The prediction logits for the next node prediction step. Will be None if mode is 'edge'.
        :return edge_logits: The prediction logits for the next edge prediction step. Will be None if mode is 'mode'.
        """
        assert mode in ['node', 'edge', 'both'], "The mode must either be 'node', 'edge' or 'both'"
        # assert node_features.shape[1] <= self.max_graph_nodes or one_step_prediction  # Check graph not too large.
        # assert torch.all(self.edge_pred_band | self.edge_pred_band.T | (edge_feat == 0))  # Check bandwidth not high.

        N = node_features.shape[1]  # The number of nodes in the batch. (including padding)

        # Convert to one-hot in case LongTensors were given.
        node_feat, edge_feat = self.categorical_to_one_hot(node_features, edge_features)

        ### GET GRAPH EMBEDDINGS
        node_feat, edge_feat = node_feat.unsqueeze(1), edge_feat.unsqueeze(1)
        if one_step_prediction:
            # We must normalize the outputs with the size of the graphs at any particular step.
            # For one-step-prediction we assume the input was not padded, meaning N truly is the graph size.
            node_normalization = torch.tensor([N], device=node_feat.device)
            edge_normalization = node_normalization
            assert mode == 'node' or edge_ji is not None, 'If doing one-step edge prediction, you must give ji.'
            graph_emb, node_emb = self.embed_graph(node_feat, edge_feat)
            # graph_emb is size (batch, 1, H), node_emb is size (batch, 1, N, H)
        else:
            node_normalization = self.masked_node_normalization  # (N, ) containing [1, 1, 2, 3, ..., N]
            edge_normalization = self.masked_edge_normalization  # (repeat-N, )
            edge_ji = self.edge_pred_ji  # (repeat-N, 2)
            graph_emb, node_emb = self.embed_graph(*self.apply_full_prediction_masks(node_feat, edge_feat))
            # graph_emb, h_i, is size (batch, repeat, H), node_emb, H_ij, is size (batch, repeat, N, H)


        ### DO PREDICTIONS
        node_logits = None
        if mode in ['node', 'both']:
            # Give the h_i (graph embeddings) for the node prediction steps, along with the graph sizes.
            node_logits = self.predict_node(graph_emb[:, :N], properties, node_normalization) # (batch, N, 1, K)

        edge_logits = None
        if mode in ['edge', 'both']:
            if not one_step_prediction:
                # In case we are using masks, the last repeat-N steps relate to edge generation.
                graph_emb = graph_emb[:, N:]  # (batch, repeat-N, H)
                node_emb = node_emb[:, N:]  # (batch, repeat-N, N, H)

            # Expand link index to match in batching dimension. (batch, repeat-N, 2, H)
            index = edge_ji[None, :, :, None].expand(node_emb.shape[0], -1, -1, node_emb.shape[-1])
            # We have that edge_normalization is equal to (edge_ji[:, 1] + 1), since i is always last node added.

            node_emb = node_emb.expand(-1, index.shape[1], -1, -1)  # (batch, 1/repeat-N/?, N, H) to broadcast with ji.
            graph_emb = graph_emb.expand(-1, index.shape[1], -1)  # (batch, 1/repeat-N/?, H)

            # Take out the node embeddings H_ii and H_ij:
            node_emb_edge = torch.gather(node_emb, dim=-2, index=index)  # (batch, repeat-N, 2, H)

            # Concat h_i onto it, so we have the edge generation features (h_i, H_ii, H_ij).
            edge_pred_feat = torch.cat([graph_emb[:, :, None, :], node_emb_edge], dim=-2)  # (batch, repeat-N, 3, H)
            # ^ Note we could also just concat in the last dimension all the features. They don't need to all be size H.
            # v Especially because we now reshape them to flatten the last two dimensions.
            edge_pred_feat = edge_pred_feat.reshape(*edge_pred_feat.shape[:-2], -1)  # (batch, repeat-N, 3H)

            if validity_masks is True:
                # If validity masks is True, we make the validity masks for you. But requires categorical input.
                assert node_features.dtype == torch.long and edge_features.dtype == torch.long
                validity_masks = self.get_validity_masks(node_features, edge_features,
                                                         edge_ji, one_step_prediction)
            elif validity_masks is False:
                validity_masks = None
            # We also allow for validity masks to be supplied. (In which case validity_masks is neither True nor False.)

            # edge_logits is size (batch, repeat-N, 1, B+1)
            edge_logits = self.predict_edge(edge_pred_feat, properties, edge_ji, edge_normalization, validity_masks)

        return node_logits, edge_logits


    def loss(self, node_logits, edge_logits, node_target, edge_target, graph_sizes=None, reduce=True, length_normalize=True):
        if len(edge_target.shape) == 3:
            # We have node_type is the target z_i.
            # Take out the target z_ij sequence (the band under the diagonal, with the graph bandwidth)
            edge_target = edge_target[:, self.edge_pred_band]

        # If graph_sizes is none, we assume it is a one-step-prediction, with no padding on the tensors.
        target_node_mask = self.node_masks[graph_sizes] if graph_sizes is not None else None
        target_edge_mask = self.target_edge_masks[graph_sizes] if graph_sizes is not None else None

        # Calculates the negative log-likelihood using a mask, such that padding nodes are ignored.
        log_likelihood_node = self.masked_loss(node_logits, node_target, target_node_mask)
        log_likelihood_edge = self.masked_loss(edge_logits, edge_target, target_edge_mask)

        if reduce:
            norm = 1
            if length_normalize:
                norm = node_logits.shape[-1] * target_node_mask.sum(-1) \
                     + edge_logits.shape[-1] * target_edge_mask.sum(-1)

            return (log_likelihood_node.sum(-1) + log_likelihood_edge.sum(-1)) / norm
        else:
            return log_likelihood_node, log_likelihood_edge

    def masked_loss(self, logits, target, mask=None):
        log_likelihood = self.criteria(logits.reshape(-1, logits.shape[-1]),  # (batch, N/repeat-N/1)
                                       target.reshape(-1)).reshape(logits.shape[:-1])
        if mask is not None:
            log_likelihood = torch.where(mask, log_likelihood, log_likelihood.new_zeros(1))

        return log_likelihood


    def batch_sample(self, num, max_nodes=48, temperature=1, ensure_connected=False, properties=None, return_graph=False):
        device = next(self.parameters()).device # torch.device('cpu')  # If on GPU, make sure not to sample more molecules at a time than fit in memory.
        if properties is not None:
            properties = properties.to(device)

        def sample_safe(logits):
            result = torch.zeros(logits.shape[:-1], dtype=torch.long, device=logits.device)
            # torch.multinomial(logits_to_probs(node_logits), 1, replacement=True).squeeze(-1)
            probs = logits_to_probs(logits)
            bad = torch.all((probs == 0) | torch.isnan(probs), -1)

            if not torch.all(bad):
                result[~bad] = torch.multinomial(probs[~bad], 1, replacement=True).squeeze(-1)
            return result

        cur_nodes = torch.zeros([num, max_nodes], dtype=torch.long, device=device)
        cur_edges = torch.zeros([num, max_nodes, max_nodes], dtype=torch.long, device=device)
        log_probs = torch.zeros([num], dtype=torch.float, device=device)
        current = torch.arange(num, dtype=torch.long, device=device)

        rw_mols = [Chem.RWMol() for _ in range(num)]
        final_lengths = [0 for _ in range(num)]
        final_log_probs = torch.zeros([num], dtype=torch.float, device=device)
        done = torch.tensor([False for _ in range(num)], dtype=torch.bool)
        bad = [False for _ in range(num)]

        for i in range(max_nodes):
            graph_size = 1 if i == 0 else i  # We encode the empty graph as graph with 1 node, which is all zero.
            ### SAMPLE AND ADD ATOM
            node_logits, _ = self.forward(cur_nodes[:, :graph_size],
                                          cur_edges[:, :graph_size, :graph_size],
                                          properties, mode='node', one_step_prediction=True)
            node_logits = node_logits.squeeze(1) / temperature  # (num, K)
            # Normalize the logits: log(p_i/(sum_j p_j)), where p_i = exp(node_logits_i), we divide by sum_j p_j:
            node_logits = node_logits - node_logits.logsumexp(dim=-1, keepdim=True)
            node_sample = sample_safe(node_logits)  # (batch, )
            # We have our sample.
            cur_nodes[:, i] = node_sample
            log_probs[~done] += torch.gather(node_logits[~done], -1, node_sample[~done, None])[:, 0]
            for idx, (rw_mol, node_idx) in enumerate(zip(rw_mols, node_sample.cpu())):
                if done[idx]:
                    continue

                node_type = self.node_types[node_idx]
                if node_type:
                    rw_mol.AddAtom(Chem.Atom(node_type))
                else:
                    # END_GRAPH was emitted!
                    final_lengths[idx] = i  # This molecule is done. Get the final length of it.
                    done[idx] = True
                    final_log_probs[idx] = log_probs[idx]
            if all(done):
                break  # All molecules are done. Exit early.
            elif i==0:
                continue  # We need two atoms to make a bond.

            # Time to sample edges
            graph_size = i + 1  # Since we have just added an atom.
            edge_total = min(i, self.max_graph_bandwidth)  # The number of edges to sample.
            start = max(0, i - self.max_graph_bandwidth)   # A node can only be connected to the most previously added.

            # Pre-compute validity masks
            valid_edge_mask = torch.ones((num, edge_total, len(self.edge_types)), dtype=torch.bool)
            for idx, rw_mol in enumerate(rw_mols):
                if done[idx]:
                    continue
                for j in range(edge_total):
                    for jdx, edge_type in enumerate(self.edge_types[1:], start=1):
                        if edge_type:  # If this is a bond type other than NO_BOND
                            rw_mol.AddBond(i, start+j, edge_type)
                            valid_edge_mask[idx, j, jdx] = check_valency(rw_mol)
                            rw_mol.RemoveBond(i, start+j)
                if torch.all(valid_edge_mask[idx, :, 1:] == 0) and ensure_connected:
                    # It may even be impossible to connect the atom. Seems extremely unlikely, but can happen in case we
                    # condition the network on properties unlike anything in the training set.
                    # print('Impossible to connect, even with ensure_connect True for', idx)
                    bad[idx] = True
                    done[idx] = True
                    final_lengths[idx] = graph_size-1
                    rw_mol.RemoveAtom(i)  # Undo the adding of this atom. (Lest the molecule not be fully connected.)
                    final_log_probs[idx] = log_probs[idx]

            if ensure_connected:
                # We can apply this to ensure there will be an edge created, no matter how improbable.
                # We get (j, i) for j in bandwidth to find the probability of no edge at all (should not happen)
                ji = torch.as_tensor([*zip(range(start, start+edge_total), [i] * edge_total)], dtype=torch.long)
                _, edge_logits_c = self.forward(cur_nodes[:, :graph_size],
                                                cur_edges[:, :graph_size, :graph_size],
                                                properties, mode='edge', one_step_prediction=True,
                                                validity_masks=valid_edge_mask.to(device), edge_ji=ji.to(device))
                edge_logits_c = edge_logits_c / temperature  # (num, edge_total, B+1)
                edge_logits_c = edge_logits_c - edge_logits_c.logsumexp(dim=-1, keepdim=True)
                if edge_total == 1:
                    # If there is only one edge, this must be the first and only edge.
                    first_edge = torch.zeros([num], dtype=torch.long)
                else:
                    # Now we have the edge distribution, assuming no bonds are made at any point.
                    bond_probs = [edge_logits_c[:, :1, 1:].logsumexp(-1),
                                  edge_logits_c[:, 1:, 1:].logsumexp(-1) + edge_logits_c[..., 0].cumsum(-1)[:, :-1]]
                    # Now we have the bond probabilities, given that there is at least 1 bond. We can sample this:
                    bond_probs = torch.cat(bond_probs, -1)
                    bond_probs = bond_probs - bond_probs.logsumexp(dim=-1, keepdim=True)

                    # first_edge = torch.multinomial(logits_to_probs(bond_probs), 1, replacement=True).squeeze(-1) # (num, )
                    first_edge = sample_safe(bond_probs) # (num, )
                    log_probs[~done] += torch.gather(bond_probs[~done], -1, first_edge[~done, None])[:, 0]

                # Modify the mask so it reflects our desired outcome.
                for idx in range(num):
                    for j in range(first_edge[idx].item()):
                        valid_edge_mask[idx, j, 1:] = False  # Before the first edge, so only allow NO_EDGE.
                    valid_edge_mask[idx, first_edge[idx], 0] = False  # The first edge. Don't allow NO_EDGE.

            valid_edge_mask = valid_edge_mask.to(device)
            connected = [False for _ in range(num)]
            for j in range(edge_total):
                ### SAMPLE AND ADD BOND
                ji = torch.tensor([[start+j, i]], device=device)
                _, edge_logits = self.forward(cur_nodes[:, :graph_size],
                                              cur_edges[:, :graph_size, :graph_size],
                                              properties, mode='edge', one_step_prediction=True,
                                              validity_masks=valid_edge_mask[:, j, None], edge_ji=ji)

                edge_logits = edge_logits.squeeze(1) / temperature  # (num, B+1)
                edge_logits = edge_logits - edge_logits.logsumexp(dim=-1, keepdim=True)
                # edge_sample = torch.multinomial(logits_to_probs(edge_logits), 1, replacement=True).squeeze(-1)
                edge_sample = sample_safe(edge_logits)
                # We have our sample.
                cur_edges[:, i, start+j] = edge_sample
                cur_edges[:, start+j, i] = edge_sample
                log_probs[~done] += torch.gather(edge_logits[~done], -1, edge_sample[~done, None])[:, 0]
                for idx, (rw_mol, edge_idx) in enumerate(zip(rw_mols, edge_sample.cpu())):
                    if done[idx]:
                        continue

                    edge_type = self.edge_types[edge_idx]
                    if edge_type:
                        rw_mol.AddBond(i, start+j, edge_type)
                        connected[idx] = True
                        # Update valid bond mask, since a new bond changes valency states.
                        for next_j in range(j+1, edge_total):
                            for jdx, edge_type in enumerate(self.edge_types[1:], start=1):
                                if edge_type:  # If this is a bond type other than NO_BOND
                                    rw_mol.AddBond(i, start+next_j, edge_type)
                                    valid_edge_mask[idx, next_j, jdx] = check_valency(rw_mol)
                                    rw_mol.RemoveBond(i, start+next_j)
                    else:
                        # NO_BOND was emitted!
                        pass
            # Check that all molecules are connected after adding edges.
            for idx, (rw_mol, conn) in enumerate(zip(rw_mols, connected)):
                if done[idx] or connected[idx]:
                    continue
                # print('This molecule ended up being disconnected', idx)
                done[idx] = True
                bad[idx] = True
                final_lengths[idx] = graph_size-1
                rw_mol.RemoveAtom(i)  # Undo the adding of this atom. (Lest the molecule not be fully connected.)
                final_log_probs[idx] = log_probs[idx]

        # ALL MOLECULES ARE DONE! (Or we reached max_nodes)
        molecules = []
        for rw_mol in rw_mols:
            mol = rw_mol.GetMol()
            assert mol is not None, 'mol was None...'
            assert check_valency(mol), 'Warning: mol is invalid'

            final_mol = convert_radical_electrons_to_hydrogens(mol)
            smiles = Chem.MolToSmiles(final_mol, isomericSmiles=False)
            assert '.' not in smiles, 'Warning: final molecule is not fully connected'

            molecules.append(smiles)

        if return_graph:
            return molecules, log_probs, bad, cur_nodes, cur_edges, torch.tensor(final_lengths, device=cur_nodes.device)

        return molecules, log_probs, bad

    def custom_sample(self, max_nodes=48, temperature=1, properties=None):
        # This is an experimental sampling method.
        if properties is None:
            properties = torch.zeros((1, 0), dtype=torch.float)

        cur_nodes = torch.zeros([1, max_nodes], dtype=torch.long)
        cur_edges = torch.zeros([1, max_nodes, max_nodes], dtype=torch.long)
        cur_log_prob = torch.zeros([1, 1], dtype=torch.long)
        cur_mol = Chem.RWMol()

        for i in range(max_nodes):
            graph_size = 1 if i == 0 else i  # We encode the empty graph as graph with 1 node, which is all zero.




    def cpu_sample(self, max_nodes=48, temperature=1, ensure_connected=True, properties=None, allow_resample=False):
        # We have a threshold since sometimes an atom can be selected, but then it turns out to be really
        # unlikely we can connect this atom to the previous 12 (as deemed by the model)
        # The reason may be that the model does not "know" which atoms are the last 12!
        threshold = 1 / 30
        thresh = torch.tensor(1 - threshold, dtype=torch.float).log()
        resamples = 0
        self.to('cpu')
        debug = False
        if debug:
            import matplotlib.pyplot as plt
            import numpy as np

        good_atoms = torch.ones((len(self.node_types), ), dtype=torch.bool)
        if properties is None:
            properties = torch.zeros((1, 0), dtype=torch.float)
        properties = properties
        cur_nodes = torch.zeros([1, max_nodes], dtype=torch.long)
        cur_edges = torch.zeros([1, max_nodes, max_nodes], dtype=torch.long)
        log_probs_node = []
        log_probs_edge = []
        cur_mol = Chem.RWMol()

        bad = False
        i = 0
        while i < max_nodes:
            graph_size = 1 if i == 0 else i  # We encode the empty graph as graph with 1 node, which is all zero.
            ### SAMPLE AND ADD ATOM
            node_logits, _ = self.forward(cur_nodes[:, :graph_size],
                                          cur_edges[:, :graph_size, :graph_size],
                                          properties, mode='node', one_step_prediction=True)
            node_logits = node_logits.squeeze(1) / temperature  # (num, K)
            # Normalize the logits: log(p_i/(sum_j p_j)), where p_i = exp(node_logits_i), we divide by sum_j p_j:
            node_logits = node_logits - node_logits.logsumexp(dim=-1, keepdim=True)
            if allow_resample:
                # In case we are resampling, do not try out the same atom twice.
                goodness = node_logits[0, good_atoms].exp().sum(-1).item()
                if goodness < threshold * threshold:
                    # Also if the probability of the remaining good atoms are very very low, just stop.
                    bad = True
                    break
                node_logits = torch.where(good_atoms[None], node_logits, node_logits.new_full((1,),
                                                                                              fill_value=float('-inf')))

            node_sample = torch.multinomial(logits_to_probs(node_logits), 1, replacement=True).squeeze(-1)  # (batch, )
            # We have our sample.
            cur_nodes[:, i] = node_sample
            log_probs_node.append(node_logits[:, node_sample])

            node_type = self.node_types[node_sample.item()]
            if node_type:
                cur_mol.AddAtom(Chem.Atom(node_type))
            else:
                # END_GRAPH was emitted!
                break

            if i==0:
                i += 1
                continue  # We need two atoms to make a bond.

            # Time to sample edges
            graph_size = i + 1  # Since we have just added an atom.
            edge_total = min(i, self.max_graph_bandwidth)  # The number of edges to sample.
            start = max(0, i - self.max_graph_bandwidth)   # A node can only be connected to the most previously added.

            # Pre-compute validity masks
            valid_edge_mask = torch.ones((1, edge_total, len(self.edge_types)), dtype=torch.bool)
            for j in range(edge_total):
                for jdx, edge_type in enumerate(self.edge_types[1:], start=1):
                    if edge_type:  # If this is a bond type other than NO_BOND
                        cur_mol.AddBond(i, start+j, edge_type)
                        valid_edge_mask[0, j, jdx] = check_valency(cur_mol)
                        cur_mol.RemoveBond(i, start+j)

            if torch.all(valid_edge_mask[0, :, 1:] == 0) and ensure_connected:
                # It may even be impossible to connect the atom. Seems extremely unlikely, but can happen in case we
                # condition the network on properties unlike anything in the training set.
                bad = True
                print("Dead end! This atom can't be connected to the molecule.")
                cur_mol.RemoveAtom(i)  # Undo the adding of this atom. (Lest the molecule not be fully connected.)
                break

            first_edge_log_prob = 0
            first_edge = 0
            ensure_bond_mask = None
            if ensure_connected:
                ensure_bond_mask = valid_edge_mask.clone()
                # We can apply this to ensure there will be an edge created, no matter how improbable.
                # We get (j, i) for j in bandwidth to find the probability of no edge at all (should not happen)
                ji = torch.as_tensor([*zip(range(start, start+edge_total), [i] * edge_total)], dtype=torch.long)
                _, edge_logits_c = self.forward(cur_nodes[:, :graph_size],
                                                cur_edges[:, :graph_size, :graph_size],
                                                properties, mode='edge', one_step_prediction=True,
                                                validity_masks=valid_edge_mask, edge_ji=ji)
                edge_logits_c = edge_logits_c / temperature  # (num, edge_total, B+1)
                edge_logits_c = edge_logits_c - edge_logits_c.logsumexp(dim=-1, keepdim=True)
                if edge_total == 1:
                    # If there is only one edge, this must be the first and only edge.
                    pass
                else:
                    # Now we have the edge distribution, assuming no bonds are made at any point.
                    # We can find the conditional distribution of bonds, given there is at least one bond.
                    no_bond_prob = edge_logits_c[:, :, 0].T  # (edge_total, num)
                    # print("Dead end!", no_bond_prob.sum().exp().item())
                    if allow_resample and no_bond_prob.sum() > thresh:
                        # print('wat', no_bond_prob.sum().exp())
                        # It is so unlikely it would likely have taken over 1000 samplings to even get a bond.
                        # In this case resample the atom.
                        resamples += 1
                        cur_mol.RemoveAtom(i)
                        # print("Resampling.", i, no_bond_prob.sum().exp())
                        log_probs_node.pop()
                        good_atoms[node_sample.item()] = False
                        if not torch.any(good_atoms):
                            print("Dead end! We are stuck resampling.", no_bond_prob.sum().exp())
                            bad = True
                            break
                        continue
                    else:
                        #print("Not resampling", i, no_bond_prob.sum().exp())
                        good_atoms.fill_(True)
                        pass
                    # inverse_log_prob = lambda log_prob: (-log_prob.expm1()).log() #  (1-log_prob.exp()).log()
                    bond_probs = [edge_logits_c[0, 0, 1:].logsumexp(-1, keepdim=True),
                                  edge_logits_c[0, 1:, 1:].logsumexp(-1) + edge_logits_c[0, :, 0].cumsum(0)[:-1]]
                    bond_probs = torch.cat(bond_probs)[None]
                    bond_probs = bond_probs - bond_probs.logsumexp(dim=-1, keepdim=True)

                    # first_edge = torch.multinomial(logits_to_probs(bond_probs), 1, replacement=True).squeeze(-1) # (num, )
                    first_edge = torch.multinomial(logits_to_probs(bond_probs), 1, replacement=True).squeeze(-1)
                    first_edge_log_prob = bond_probs[:, first_edge]

                    if debug:
                        test_text = f"Lets force a bond ({i}, {first_edge.item()}): {np.around(no_bond_prob.sum().exp().item(), 3)} {node_type}"
                        # print(test_text)
                        mol = convert_radical_electrons_to_hydrogens(cur_mol.GetMol())
                        Chem.SanitizeMol(mol)
                        plt.imshow(Draw.MolToImage(mol, kekulize=False, size=(300, 300), fitImage=False), interpolation="nearest")
                        plt.title(test_text)
                        plt.show()
                        print(np.around(edge_logits_c.softmax(-1).numpy(), 3))
                        print('First bond prob:', np.around(bond_probs.exp().numpy(), 3))


                # Modify the mask so it reflects our desired outcome.
                for j in range(first_edge):
                    ensure_bond_mask[0, j, 1:] = False  # Before the first edge, so only allow NO_EDGE.
                ensure_bond_mask[0, first_edge, 0] = False  # The first edge. Don't allow NO_EDGE.

            connected = False
            for j in range(edge_total):
                ### SAMPLE AND ADD BOND
                ji = torch.tensor([[start+j, i]])
                _, edge_logits = self.forward(cur_nodes[:, :graph_size],
                                              cur_edges[:, :graph_size, :graph_size],
                                              properties, mode='edge', one_step_prediction=True,
                                              validity_masks=valid_edge_mask[:, j, None], edge_ji=ji)

                edge_logits = edge_logits.squeeze(1) / temperature  # (num, B+1)
                edge_logits = edge_logits - edge_logits.logsumexp(dim=-1, keepdim=True)
                # edge_sample = torch.multinomial(logits_to_probs(edge_logits), 1, replacement=True).squeeze(-1)
                if ensure_bond_mask is not None:
                    edge_logits_masked = torch.where(ensure_bond_mask[:, j],
                                                     edge_logits, edge_logits.new_full((1,), fill_value=float('-inf')))
                else:
                    edge_logits_masked = edge_logits
                edge_sample = torch.multinomial(logits_to_probs(edge_logits_masked), 1, replacement=True).squeeze(-1)
                # We have our sample.
                cur_edges[:, i, start+j] = edge_sample
                cur_edges[:, start+j, i] = edge_sample
                if ensure_connected and j == first_edge:
                    # The probability if this edge is the probability that it was the first edge
                    # and that whatever edge it ended up being.
                    log_probs_edge.append(first_edge_log_prob + edge_logits[:, edge_sample])
                else:
                    log_probs_edge.append(edge_logits[:, edge_sample])


                edge_type = self.edge_types[edge_sample.item()]
                if edge_type:
                    cur_mol.AddBond(i, start+j, edge_type)
                    connected = True
                    if debug:
                        mol = convert_radical_electrons_to_hydrogens(cur_mol.GetMol())
                        Chem.SanitizeMol(mol)
                        plt.imshow(Draw.MolToImage(mol, highlightAtoms=[start + j],
                                                   kekulize=False, size=(300, 300), fitImage=False),
                                   interpolation="nearest")
                        plt.title(f'Bond prob ({i}, {j}, {edge_sample.item()}) {edge_logits.softmax(-1)[:, edge_sample].item()}')
                        plt.show()
                        print(f'bond {i}, {j} {np.around(edge_logits.softmax(-1), 3)}')
                    # Update valid bond mask, since a new bond changes valency states.
                    for next_j in range(j+1, edge_total):
                        for jdx, edge_type in enumerate(self.edge_types[1:], start=1):
                            if edge_type:  # If this is a bond type other than NO_BOND
                                cur_mol.AddBond(i, start+next_j, edge_type)
                                valid_edge_mask[0, next_j, jdx] = check_valency(cur_mol)
                                cur_mol.RemoveBond(i, start+next_j)
                else:
                    if debug:
                        print(f'No bond {i}, {j} {np.around(edge_logits.softmax(-1), 3)}')
                    # NO_BOND was emitted!
                    pass
            # Check that all molecules are connected after adding edges.
            if not connected:
                bad = True
                print('disconnect')
                cur_mol.RemoveAtom(i)  # Undo the adding of this atom. (Lest the molecule not be fully connected.)
                break
            i += 1

        # ALL MOLECULES ARE DONE! (Or we reached max_nodes)
        mol = cur_mol.GetMol()
        assert mol is not None, 'mol was None...'
        assert check_valency(mol), 'Warning: mol is invalid'

        final_mol = convert_radical_electrons_to_hydrogens(mol)
        Chem.SanitizeMol(final_mol)
        smile = Chem.MolToSmiles(final_mol, isomericSmiles=False)
        assert '.' not in smile, 'Warning: final molecule is not fully connected'

        if log_probs_node:
            log_probs_node = torch.stack(log_probs_node)
        if log_probs_edge:
            log_probs_edge = torch.stack(log_probs_edge)
        return smile, bad, final_mol, cur_nodes, cur_edges,\
               log_probs_node, log_probs_edge, resamples


def initialize_masks(max_node_unroll, max_edge_unroll):
    """
    Returns:
        node_masks: node mask for each step
        adj_masks: adjacency mask for each step
        is_node_update_mask: 1 indicate this step is for updating node features
        flow_core_edge_mask: get the distributions we want to model in adjacency matrix
    Note: Copied from GraphAF with minor changes.
    """
    # https://cs.stanford.edu/people/jure/pubs/graphrnn-icml18.pdf
    num_masks = (max_node_unroll # Number of nodes.
                + ((max_edge_unroll - 1) * max_edge_unroll) // 2  # Number of elements under the diagonal. I.e. the number of possible bonds.
                + (max_node_unroll - max_edge_unroll) * max_edge_unroll)

    num_mask_edge = num_masks - max_node_unroll
    node_masks1 = torch.zeros([max_node_unroll, max_node_unroll]).bool()
    adj_masks1 = torch.zeros([max_node_unroll, max_node_unroll, max_node_unroll]).bool()
    node_masks2 = torch.zeros([num_mask_edge, max_node_unroll]).bool()
    adj_masks2 = torch.zeros([num_mask_edge, max_node_unroll, max_node_unroll]).bool()

    # is_node_update_masks = torch.zeros([num_masks]).byte()

    edge_pred_ij = torch.zeros([num_mask_edge, 2]).long()

    edge_pred_band = torch.zeros([max_node_unroll, max_node_unroll]).bool()

    # masks_edge = dict()
    cnt = 0
    cnt_node = 0
    cnt_edge = 0
    for i in range(max_node_unroll):
        node_masks1[cnt_node][:i] = 1
        adj_masks1[cnt_node][:i, :i] = 1
        # is_node_update_masks[cnt] = 1
        cnt += 1
        cnt_node += 1
        start = max(0, i - max_edge_unroll)
        edge_total = min(i, max_edge_unroll)
        for j in range(edge_total):
            if j == 0:
                node_masks2[cnt_edge][:i + 1] = 1
                adj_masks2[cnt_edge] = adj_masks1[cnt_node - 1].clone()
                adj_masks2[cnt_edge][i, i] = 1
            else:
                node_masks2[cnt_edge][:i + 1] = 1
                adj_masks2[cnt_edge] = adj_masks2[cnt_edge - 1].clone()
                adj_masks2[cnt_edge][i, start + j - 1] = 1
                adj_masks2[cnt_edge][start + j - 1, i] = 1
            cnt += 1
            cnt_edge += 1
    # print('\n'.join(map(str, list(adj_masks2.long().numpy()))))
    assert cnt == num_masks, 'masks cnt wrong'
    assert cnt_node == max_node_unroll, 'node masks cnt wrong'
    assert cnt_edge == num_mask_edge, 'edge masks cnt wrong'


    cnt = 0
    for i in range(max_node_unroll):
        if i < max_edge_unroll:
            start = 0
            edge_total = i
        else:
            start = i - max_edge_unroll
            edge_total = max_edge_unroll

        for j in range(edge_total):
            edge_pred_ij[cnt][0] = start + j
            edge_pred_ij[cnt][1] = i
            cnt += 1
    assert cnt == num_mask_edge, 'edge mask initialize fail'

    for i in range(max_node_unroll):
        if i == 0:
            continue
        if i < max_edge_unroll:
            start = 0
            end = i
        else:
            start = i - max_edge_unroll
            end = i
        edge_pred_band[i][start:end] = 1

    node_masks = torch.cat((node_masks1, node_masks2), dim=0)
    adj_masks = torch.cat((adj_masks1, adj_masks2), dim=0)

    # We have to set max_node_unroll + 1 here because of a bug. This should also be fixed if the max_graph_size is+2
    target_edge_masks = torch.zeros([max_node_unroll, num_mask_edge]).bool()  # This was not part of GraphAF.
    for i, length in enumerate(edge_pred_band.sum(-1).cumsum(-1)):
        target_edge_masks[i, :length] = 1
    # target_edge_masks[-1] = 1
    return node_masks, adj_masks, edge_pred_ij, target_edge_masks, edge_pred_band


### Helper functions
def mol2graph(smiles='c1ccccc1'):
    m = Chem.MolFromSmiles(smiles)
    Chem.Kekulize(m)
    # If adj is not kekulized, then some entries in adj may be 1.5.
    adj = torch.as_tensor(Chem.GetAdjacencyMatrix(m, useBO=True), dtype=torch.long)
    # torch_sparse.SparseTensor.from_dense(adjmat)

    # We can permute inputs randomly when training.
    # Then when predicting we can predict from different permutations and average predictions, i.e. model averaging.
    #  Meaning on the third atom prediction, we can try permute the two previous.

def check_valency(mol):
    """
    Checks that no atoms in the mol have exceeded their possible
    valency
    :return: True if no valency issues, False otherwise
    """
    try:
        Chem.SanitizeMol(mol,
                         sanitizeOps=Chem.SanitizeFlags.SANITIZE_PROPERTIES)
        return True
    except ValueError:
        return False

def check_chemical_validity(mol):
    """
    Checks the chemical validity of the mol object. Existing mol object is
    not modified. Radicals pass this test.
    :return: True if chemically valid, False otherwise
    """
    s = Chem.MolToSmiles(mol, isomericSmiles=True)
    m = Chem.MolFromSmiles(s)  # implicitly performs sanitization
    if m:
        return True
    else:
        return False

def convert_radical_electrons_to_hydrogens(mol):
    """
    Converts radical electrons in a molecule into bonds to hydrogens. Only
    use this if molecule is valid. Results a new mol object
    :param mol: rdkit mol object
    :return: rdkit mol object
    """
    m = copy.deepcopy(mol)
    if Chem.Descriptors.NumRadicalElectrons(m) == 0:  # not a radical
        return m
    else:  # a radical
        print('converting radical electrons to H')
        for a in m.GetAtoms():
            num_radical_e = a.GetNumRadicalElectrons()
            if num_radical_e > 0:
                a.SetNumRadicalElectrons(0)
                a.SetNumExplicitHs(num_radical_e)
    return m





if __name__ == "__main__":
    from rdkit import Chem
    m = Chem.MolFromSmiles('c1ccccc1')
    Chem.Kekulize(m)
    import torch_sparse

    adjmat = torch.as_tensor(Chem.GetAdjacencyMatrix(m, useBO=True), dtype=torch.long) #, dtype=torch.float)
    adjmat = torch_sparse.SparseTensor.from_dense(adjmat)

    #edge_index, edge_weight = geom.utils.dense_to_sparse(adjmat)

    # from the nevae code, the edge weight is actually the bond type, i.e. 1, 1.5, 2, 3, ...
    # it should really just be a one-hot featureized vector, which would be applied in the messaging step.

    #print(edge_weight.shape, edge_index.shape)

    # atoms = ['C', 'H', 'O', 'N']
    atoms = ['C', 'N', 'O', 'F', 'P', 'S', 'Cl', 'Br', 'I']
    atoms = {C:i for i, C in enumerate(atoms)}
    nodes = torch.as_tensor( [atoms[atom.GetSymbol()] for atom in m.GetAtoms()], dtype=torch.long )
    x = nn.functional.one_hot(nodes, num_classes=len(atoms)).float()

    graphsize = 40
    xpadded = x.new_zeros((1, graphsize, x.shape[1]))
    xpadded[0, :x.shape[0], :x.shape[1]] = x
    MolecularCNN()(xpadded, adjmat.sparse_resize([graphsize, graphsize]).to_dense()[None], torch.LongTensor((6,)))

    g = MolecularCNN()

    g.log_prob(xpadded, adjmat.sparse_resize([graphsize, graphsize]).to_dense()[None], torch.LongTensor((6,)))


    '''' 
    exit()
    conv2 = GraphAFConv(x.shape[-1], 2, 3, 1)
    print(conv2(x, adjmat, adjmat.storage._value-1))

    net = RGCN(x.shape[-1])
    print(net(x, adjmat, adjmat.storage._value-1).shape)
    '''
