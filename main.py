from argparse import ArgumentParser
import os

import numpy as np
import pytorch_lightning as pl
import torch
import torch.nn.functional as F
import torch.utils.data as data
from rdkit import Chem
from rdkit.Chem import Draw
from tqdm import tqdm

from dataset import SmilesDataset, split_dataset, MCNNInput
from model import MolecularCNN

def text_phantom(canvas, text):
    from PIL import ImageDraw, ImageFont
    # Availability is platform dependent
    font = 'arial'
    size = 24

    # Create font
    pil_font = ImageFont.truetype(font + ".ttf", size=size, encoding="unic")
    text_width, text_height = pil_font.getsize(text)

    # draw the text onto the canvas
    draw = ImageDraw.Draw(canvas)
    offset = ((size - text_width) // 2,
              (size - text_height) // 2)
    white = "#000000"
    draw.text(offset, text, font=pil_font, fill=white)

    # Convert the canvas into an array with values in [0, 1]
    return np.asarray(canvas)

class Model(pl.LightningModule):
    def __init__(self, hparams):
        super().__init__()
        self.all_train_smiles = set()  # The molecules seen under training. (Used to find novelty)
        # FIXME: ^ Figure out how to make part of the persistent state. (Maybe state_dict?)
        self.hparams = hparams
        if self.hparams.conditioning and isinstance(self.hparams.conditioning, str):
            # Fix that the list is loaded as one string.
            self.hparams.conditioning = self.hparams.conditioning.strip('[]').replace("'", '').replace('"', '').replace('\\', '').replace(',', ' ').split()
        if not hasattr(self.hparams, 'test_dataset_path'):
            # We only implemented the test dataset after having trained the final models.
            # So we need to set this value for these models, as they were not saved with this.
            self.hparams.test_dataset_path = os.path.join('dataset', 'MOSES_dataset_v1_test.smi')

        if self.hparams.num_workers is None:
            num_workers = os.cpu_count()
            print('num_workers argument not given. Using', num_workers, 'workers')
            self.hparams.num_workers = num_workers

        # SETUP TRAINING DATASET
        dataset = SmilesDataset(self.hparams.dataset_path,
                                conditioning=self.hparams.conditioning,
                                validity_masks=not self.hparams.no_validity_masks and
                                               not self.hparams.validity_masks_from_model)
        self._train_dataset, self._val_dataset, _ = split_dataset(dataset, val_prop=0.05, test_prop=0, seed=hparams.seed)
        if False:
            # Train on single example! (Good way to debug, or swiftly test out ideas.)
            self._train_dataset = data.Subset(self._train_dataset, [0] * 10000)
            self._val_dataset = self._train_dataset

        # SET UP TEST DATASET
        test_dataset = SmilesDataset(self.hparams.test_dataset_path,
                                     conditioning=self.hparams.conditioning,
                                     validity_masks=not self.hparams.no_validity_masks and
                                                    not self.hparams.validity_masks_from_model)
        self._test_dataset = test_dataset

        self.model = MolecularCNN(dataset.atom_types, dataset.bond_types, num_properties=len(dataset.conditioning),
                                  max_num_nodes=dataset.max_num_atoms, max_graph_bandwidth=dataset.max_graph_bandwidth,
                                  hidden_size=self.hparams.hidden_size, dropout=self.hparams.dropout_prob,
                                  batchnorm=not self.hparams.no_batchnorm)

        self.model.graph_relevancy.p = self.hparams.graph_relevancy
        self.model.property_relevancy.p = self.hparams.property_relevancy
        self.model.edge_step_relevancy.p = self.hparams.edge_step_relevancy

        if self.hparams.gen_max_nodes is None:
            self.hparams.gen_max_nodes = dataset.max_num_atoms

    def forward(self, batch : MCNNInput):
        batch = MCNNInput(*batch)

        validity_masks = not self.hparams.no_validity_masks
        if validity_masks and batch.validity_masks is not None:
            validity_masks = batch.validity_masks # We can let the masks be generated in the dataloader.

        node_logits, edge_logits = self.model(batch.node_type, batch.adj_type,
                                              batch.properties, mode='both', validity_masks=validity_masks)

        # print(' ')
        # print(np.around(node_logits[:, :4].softmax(-1).mean(0).detach().cpu().numpy(),  3))
        # print(np.around(node_logits.softmax(-1).mean(0).mean(0).detach().cpu().numpy(), 3))

        loss = self.model.loss(node_logits, edge_logits, batch.node_type, batch.adj_type,
                          batch.graph_size, length_normalize=not self.hparams.no_length_normalization)

        return node_logits, edge_logits, loss

    def training_step(self, batch, batch_idx):
        batch = MCNNInput(*batch)
        self.all_train_smiles.update(batch.smile)  # Add all the smiles we have seen during training to all_train_smiles.
        # TODO: Make batch into a dict, so we can just do if 'validity_masks' in batch (which it only be if)

        node_logits, edge_logits, batch_loss = self.forward(batch)
        loss = batch_loss.mean(0) # Take the mean over the batch.
        logger_logs = {'Loss/train': loss.item()}

        # import math
        # self.model.graph_relevancy.p = 1.0/(1.0+math.exp(-(400-self.global_step)/40.0))

        # Try with self.logger.experiment.add_histogram
        # avg_node_logits
        if 0 < self.hparams.log_level and False:
            n = min(max(len(self._train_dataset) // 10, 100), 5000)
            if self.global_step % n == 0 and loss < 0.04:
                # The model needs a little training, or sampling will take longer than necessary.
                with torch.no_grad():
                    self.do_plots(batch, node_logits, edge_logits, batch_loss)
                    '''
                    if n <= self.global_step:
                        device = next(self.model.parameters()).device
                        self.model.cpu()
                        # The model needs to be trained somewhat, or this will take more time.
                        molecules, _, _, _, progress_bar = self.sample(len(batch.properties), prior=batch.properties)
                        logger_logs.update({'Gen/train_' + name: value for name, value in progress_bar.items()})
                        self.model.to(device)
                        newline = "  \n"
                        self.logger.experiment.add_text('Gen/train_smiles', f'<pre>{newline.join(molecules)}</pre>',
                                                        global_step=self.global_step)

                        smile2img = lambda smile: Draw.MolToImage(Chem.MolFromSmiles(smile), kekulize=False)
                        molimgs = [np.array(smile2img(smile)) for idx, smile in enumerate(molecules[:16])]
                        self.logger.experiment.add_images('Gen/train', np.stack(molimgs) / 255,
                                                          global_step=self.global_step, dataformats='NHWC')
                    '''


        return {'loss': loss * self.hparams.accumulate_grad_batches,
                'log': logger_logs}

    def configure_optimizers(self):
        return torch.optim.Adam(list(filter(lambda p: p.requires_grad, self.model.parameters())), lr=self.hparams.lr)


    def train_dataloader(self):
        return data.DataLoader(self._train_dataset, batch_size=self.hparams.batch_size,
                               num_workers=self.hparams.num_workers, shuffle=True, pin_memory=True)

    def val_dataloader(self):
        return data.DataLoader(self._val_dataset, batch_size=self.hparams.batch_size,
                               num_workers=self.hparams.num_workers, shuffle=False, pin_memory=True)  # No reason to shuffle.

    def test_dataloader(self):
        # Shuffle just to be sure.
        return data.DataLoader(self._test_dataset, batch_size=self.hparams.batch_size,
                               num_workers=self.hparams.num_workers, shuffle=True, pin_memory=True)

    def validation_step(self, batch, batch_idx):
        node_type, adj_type, graph_sizes, properties, smiles, *_ = batch

        _, _, loss = self.forward(batch)

        return {'val_loss': loss}

    def sample(self, num=256, prior=None):
        # Since we do sampling un-batched, it is faster on CPU.

        molecules = []
        invalid = []
        unique = set()
        novel = []

        properties = None
        if isinstance(prior, torch.Tensor) and self.model.num_properties != 0:
            properties = prior
        if self.model.num_properties == 0 or isinstance(prior, torch.Tensor):
            prior = range(1)  # Just a placeholder, so we don't have to branch code paths.

        progress_bar = {}
        bar = tqdm(total=num)
        while len(molecules) < num:
            for batch in prior:
                if isinstance(batch, tuple):
                    properties = MCNNInput(*batch).properties
                if properties is not None:
                    sample_cnt = len(properties)
                else:
                    sample_cnt = self.hparams.batch_size

                sample, _, bad = self.model.batch_sample(sample_cnt,
                                                         max_nodes=self.hparams.gen_max_nodes,
                                                         temperature=self.hparams.gen_temperature,
                                                         properties=properties)
                sample = sample # Let's sample more than needed.  # [:num - len(molecules)] Let's not sample more than we need.
                good = [molecule for molecule, fail in zip(sample, bad) if not fail]
                bad = [molecule for molecule, fail in zip(sample, bad) if fail]
                molecules += good
                invalid += bad
                unique.update(good)
                novel += [smile for smile in good if smile not in self.all_train_smiles]
                if molecules:
                    progress_bar['unique'] = len(unique) / len(molecules)
                    progress_bar['valid'] = 1 - len(invalid) / (len(molecules) + len(invalid))
                    progress_bar['novelty'] = len(novel) / len(molecules)
                    bar.set_postfix(progress_bar)
                bar.update(len(good))

        return molecules, unique, invalid, novel, progress_bar

    def validation_epoch_end(self, outputs):
        logger_logs = {}

        val_loss_mean = torch.cat([x['val_loss'] for x in outputs], dim=0).mean()
        logger_logs['Loss/val'] = val_loss_mean

        if self.global_step > 100 and val_loss_mean < 0.045 and self.hparams.log_level and False:
            # The model needs to be trained somewhat, or this will take more time.
            molecules, _, _, _, progress_bar = self.sample(256, prior=self.val_dataloader())
            logger_logs.update({'Gen/val_' + name: value for name, value in progress_bar.items()})

            if self.logger is not None:
                newline = "  \n"
                self.logger.experiment.add_text('Gen/val_smiles', f'<pre>{newline.join(molecules)}</pre>',
                                                global_step=self.global_step)
            smile2img = lambda smile: Draw.MolToImage(Chem.MolFromSmiles(smile), kekulize=False, fitImage=True)
            molimgs = [np.array(smile2img(smile)) for idx, smile in enumerate(molecules[:16])]
            self.logger.experiment.add_images('Gen/Val', np.stack(molimgs) / 255,
                                              global_step=self.global_step, dataformats='NHWC')

        # progress_bar['geom_avg'] = (unique * valid * novelty) ** (1/3)  # Could monitor this for early stopping.

        # Add progress bar items to log.
        return {'val_loss': val_loss_mean,
                'progress_bar': logger_logs,
                'log': logger_logs}

    def do_plots(self, batch, node_logits, edge_logits, batch_loss):
        import numpy as np
        import matplotlib.pyplot as plt
        from sklearn import (manifold, decomposition)

        node_type, adj_type, graph_sizes, properties, smiles, *_ = batch

        # We find the distribution of the loss w.r.t. to the node/edge types.
        adj_type_band = adj_type[:, self.model.edge_pred_band]

        # If graph_sizes is none, we assume it is a one-step-prediction, with no padding on the tensors.
        target_node_mask = self.model.node_masks[graph_sizes]
        target_edge_mask = self.model.target_edge_masks[graph_sizes]

        # Calculates the sum-reduction, such that padding nodes are ignored.
        node_loss = self.model.masked_loss(node_logits, node_type, target_node_mask)
        adj_loss = self.model.masked_loss(edge_logits, adj_type_band, target_edge_mask)

        def get_distribution(loss, type, names):
            one_hot = F.one_hot(type, num_classes=len(names))
            loss = (one_hot * loss[..., None]).sum(1)  # Sum over graph (E.g. 3 atoms losses)
            cnt = one_hot.sum(1)  # Now we want the avg. atom loss for every specific atom type present.
            indices = cnt != 0
            loss[indices] = loss[indices] / cnt[indices]
            loss = loss.mean(0).cpu() / len(names)  # As the loss is the sum of e.g. 9 atom types, normalize.
            return {str(name): loss[idx].item() for idx, name in enumerate(names)}

        # Log the loss distribution of error on the different target types.
        if 2 < self.hparams.log_level:
            node_loss_dist = get_distribution(node_loss, node_type, self.model.node_types)
            self.logger.experiment.add_scalars('NodeLoss', node_loss_dist, self.global_step)  # LOG IT
            adj_loss_dist = get_distribution(adj_loss, adj_type_band, self.model.edge_types)
            self.logger.experiment.add_scalars('EdgeLoss', adj_loss_dist, self.global_step)

        def get_matrix_fig(mean_node_loss, mean_edge_loss):
            mean_node_loss = mean_node_loss.cpu()
            mean_edge_loss = mean_edge_loss.cpu()
            matrix = np.zeros((self.model.max_num_nodes,) * 2, dtype=np.float)
            for i in range(self.model.max_num_nodes):
                matrix[i, i] = mean_node_loss[i]
            for i, (x, y) in enumerate(self.model.edge_pred_ji.clone().cpu()):
                matrix[y, x] = mean_edge_loss[i]

            fig = plt.figure()
            ax = plt.gca()
            im = ax.matshow(matrix)

            from mpl_toolkits.axes_grid1 import make_axes_locatable
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="5%", pad=0.05)
            cbar = plt.colorbar(im, cax=cax)
            cbar.ax.get_yaxis().labelpad = 15
            cbar.ax.set_ylabel('Loss', rotation=270)
            plt.tight_layout()  # TODO: Check, does this help?
            return fig

        # The loss distribution over the z_i and z_ij
        unnormalized_loss_matrix = get_matrix_fig(node_loss.mean(0), adj_loss.mean(0))  # Mean over batch.
        self.logger.experiment.add_figure('LossMatrix', unnormalized_loss_matrix, self.global_step)
        self.logger.experiment.add_figure('LossMatrixExp', get_matrix_fig((-node_loss.mean(0)).exp(),
                                                                          (-adj_loss.mean(0)).exp()), self.global_step)

        normal_node_loss = (node_loss / graph_sizes[:, None])  # Avg. over actual number of nodes in graph first.
        # We need the number of edge predictions required for each graph (function of the graph size)
        num_masks = ((self.model.max_graph_bandwidth - 1) * self.model.max_graph_bandwidth) // 2 \
                    + (graph_sizes - self.model.max_graph_bandwidth) * self.model.max_graph_bandwidth
        num_masks[num_masks == 0] = 1  # In case a molecule is one atom, there's no edge prediction.
        normal_adj_loss = (adj_loss / num_masks[:, None])
        normalized_loss_matrix = get_matrix_fig(normal_node_loss.mean(0) / len(self.model.node_types),
                                                normal_adj_loss.mean(0) / len(self.model.edge_types))
        self.logger.experiment.add_figure('NormalizedLossMatrix', normalized_loss_matrix, self.global_step)

        node_feat, edge_feat = self.model.categorical_to_one_hot(node_type, adj_type)

        node_pred = node_logits.softmax(-1).cpu()

        def plot_pred_dist(pred, true, names, title):
            centers = np.array(range(len(names)))
            width = 0.7

            fig = plt.figure()
            ax = plt.gca()
            rects1 = ax.bar(centers, pred, width=width - width / 8, zorder=1)
            rects2 = ax.bar(centers + width / 8, true, width=width, zorder=0)

            ax.set_xticks(centers)
            ax.set_xticklabels(list(map(str, names)))
            ax.set_title(title)
            ax.legend((rects2[0], rects1[0]), ('True', 'Pred'))
            ax.set_ylim([0, 1])
            plt.tight_layout()
            return fig

        pred_dist0 = plot_pred_dist(node_pred.mean(0)[0].cpu(), node_feat.mean(0)[0].cpu(),
                                    self.model.node_types, 'Prediction distribution (step 0)')
        self.logger.experiment.add_figure('Pred0', pred_dist0, self.global_step)

        last_idx = graph_sizes[:, None, None].expand(-1, 1, node_pred.shape[-1]).cpu()
        pred_distN = plot_pred_dist(pred=torch.gather(node_pred.cpu(), dim=1, index=last_idx).mean(0)[0],
                                    true=[1] + [0] * len(self.model.node_types[1:]),
                                    names=self.model.node_types, title='Prediction distribution (last step)')
        self.logger.experiment.add_figure('PredN', pred_distN, self.global_step)

        if 2 < self.hparams.log_level:
            self.logger.experiment.add_histogram('GraphEnd/Pred', node_feat.mean(0)[:, 0].nonzero().cpu(), global_step=self.global_step )
            self.logger.experiment.add_histogram('GraphEnd/True', graph_sizes.cpu(), global_step=self.global_step )

        # Plot embeddings in with dimensionality reduction.
        graph_emb, _ = self.model.embed_graph(node_feat[:, None], edge_feat[:, None])
        graph_emb = graph_emb.cpu()

        def plot_embedding(X, title=None):
            x_min, x_max = np.min(X, 0), np.max(X, 0)
            X = (X - x_min) / (x_max - x_min)

            fig = plt.figure()
            ax = plt.gca()
            ax.scatter(X[:, 0], X[:, 1])
            for i in range(X.shape[0]):
                ax.text(X[i, 0], X[i, 1], i,
                        # color=plt.cm.Set1(y[i] / 10.),
                        fontdict={'weight': 'bold', 'size': 9})

            plt.xticks([]), plt.yticks([])
            if title is not None:
                ax.title(title)
            return fig

        self.logger.experiment.add_figure('Visual/PCA',
                                          plot_embedding(
                                              decomposition.PCA(n_components=2).fit_transform(graph_emb[:, 0])),
                                          self.global_step)

        self.logger.experiment.add_figure('Visual/TSNE8', plot_embedding(
            manifold.TSNE(perplexity=8, n_components=2, init='pca', random_state=0).fit_transform(graph_emb[:, 0])))

        self.logger.experiment.add_figure('Visual/TSNE2', plot_embedding(
            manifold.TSNE(perplexity=2, n_components=2, init='pca', random_state=0).fit_transform( graph_emb[:, 0])))

        smile2img = lambda smile: Draw.MolToImage(Chem.MolFromSmiles(smile), kekulize=False, fitImage=True)
        molimgs = [np.array(smile2img(smile)) for idx, smile in enumerate(smiles)]

        self.logger.experiment.add_images('Visual/Mols', np.stack(molimgs) / 255,
                                          global_step=self.global_step, dataformats='NHWC')

    @staticmethod
    def add_model_specific_args(parent_parser):
        parser = ArgumentParser(parents=[parent_parser], add_help=False)
        parser.add_argument('--no_validity_masks', action='store_true', default=False)
        parser.add_argument('--batch_size', default=32, type=int)
        parser.add_argument('--hidden_size', default=1024, type=int)
        parser.add_argument('--no_length_normalization', action='store_true', default=False,
                            help='If the loss should not be normalized with respect to the size of the graph.')
        parser.add_argument('--lr', default=1e-3, type=float)

        parser.add_argument('--no_batchnorm', action='store_true', default=False)  # Batchnorm in prednets?
        parser.add_argument('--dropout_prob', default=0, type=float)  # Dropout regularization.
        parser.add_argument('--graph_relevancy', default=0, type=float)  # Graph features are (of course) very important
        parser.add_argument('--property_relevancy', default=0, type=float)
        parser.add_argument('--edge_step_relevancy', default=0, type=float)  # Edge step is very important!

        # Some arguments for molecule generation.
        parser.add_argument('--gen_max_nodes', default=None, type=int)
        parser.add_argument('--gen_temperature', default=1, type=float)
        parser.add_argument('--conditioning', nargs='*', default=[],
                            choices=tuple(SmilesDataset.properties.keys()),
                            help='Which properties to use for property conditioning.')

        return parser

def general_args():
    parser = ArgumentParser()
    # TODO: Figure out how to use hparams like --validity_masks=True instead of this double negation stuff.
    # General training parameters
    parser.add_argument('--test', action='store_true', default=False,
                        help="If the model should be evaluated on test set")
    parser.add_argument('--seed', default=42, type=int,
                        help='Set the seed for the RNG. This may change the training split, but never the test split.')
    parser.add_argument('--num_workers', default=None, type=int,
                        help='Number of workers in DataLoader. Should not exceed your number of processors.')
    parser.add_argument('--log_level', default=0, type=int,
                        help='The level of training logs to save (mainly Tensorboard logs). Set to 0/-1 to disable.')
    parser.add_argument('--dataset_path', default=os.path.join('dataset', 'MOSES_dataset_v1_train.smi'))
    parser.add_argument('--test_dataset_path', default=os.path.join('dataset', 'MOSES_dataset_v1_test.smi'))

    # Dataset arguments:
    parser.add_argument('--validity_masks_from_model', action='store_true', default=False,
                        help='If the validity masks should be created in the model and not the dataloader')

    return parser

if __name__ == "__main__":
    parser = general_args()

    # Hyperparameters
    parser = Model.add_model_specific_args(parser)

    parser = pl.Trainer.add_argparse_args(parser)  # Add trainer options.

    # Common parameters to use:
    # --gpus=0 --hidden_size=1024 --log_level=2 --overfit_pct=0.125
    # --batch_size=32 --accumulate_grad_batches=1  # Setting accumulate_grad_batches higher give a bigger batch size.
    # --validity_masks_from_dataloader --num_workers=8 # These together speed up training a lot.

    # Parse args and start training.
    args = parser.parse_args()

    if args.resume_from_checkpoint:
        print('Loading model', args.resume_from_checkpoint)

    # Log level is also a Macro, used to swiftly enable/disable some flags:
    if args.log_level == 0:
        args.weights_summary = None
        args.num_sanity_val_steps = 0
    elif 1 < args.log_level:  # More verbose logging.
        args.log_gpu_memory = True
        args.track_grad_norm = 2


    model = Model(args)
    trainer = pl.Trainer.from_argparse_args(args)
    if not args.test:
        trainer.fit(model)
    else:
        trainer.test(model)
