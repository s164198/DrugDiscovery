#!/bin/sh
wget https://media.githubusercontent.com/media/molecularsets/moses/master/data/dataset_v1.csv
tail dataset_v1.csv -n+2 | awk -F ',' '{print $1 > ("MOSES_dataset_v1_"$2".smi")}'