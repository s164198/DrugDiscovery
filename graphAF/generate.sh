CUDA_VISIBLE_DEVICES=3 python3 -u -W ignore train.py --path ./data_preprocessed/zinc250k_clean_sorted \
    --gen --gen_out_path ./mols/test_100mol.txt \
    --batch_size 32 --lr 0.001 --epochs 100 \
    --shuffle --deq_coeff 0.9 --save --name l12_h128_o128_exp_sbatch \
    --num_flow_layer 1 --nhid 128 --nout 128 --gcn_layer 3 \
    --is_bn --divide_loss --st_type exp \
    --init_checkpoint ./good_ckpt/checkpoint277 \
    --gen_num 10000 --min_atoms 5 --save --seed 66666666 --temperature 0.7