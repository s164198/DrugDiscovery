CC1=CC=CC=C1
CC1CCCC1
CCC(CN(C(=O)C1=CC=NC=C1)C(=O)C1=CC=C(C)C=C1)=NC
C#CCC=N
CONNC(=O)CI
CC(C)CN(C)CCCCC(CBr)CNN
CCNC(C)C1=CC=CC=C1
CCC1OC2=C(Br)C=CC(=C2)CCNC(C(O)CNC(=O)O)CC=CC(F)=CC=C(C=C(F)I)[PH]1=O
CCCCCC
O=C(NC1=C(I)C=CC=C1)C1=CC=CC=C1
CS(=O)(=O)CC(OC(CC(P)CP)C(N)=O)C(F)I
CC(C(=O)CNC1=CC=CC(F)=C1)C1=CC(O)=CC=C1
O=C1CCN(CCI)CCCN1
O=C(NCC(CBr)CC(CC(O)O)C(O)O)C1=CC=CC(F)=C1
ClC1CCC[PH](I)=CC=C2SC=CCC(CC1)C2=NN=NI
CC1=CC=CC(Cl)=C1
CCNCCC(C)C1=CC=CC=C1
C#CS(=O)CCN(CCCN)C(Cl)C(F)=[PH](O)Br
CNC(=O)CC1CC(SC(C=NO)=CSS)CCSC2=CC=C1C=C2
CCCCNC(=O)CCN(C)CCC
NC=C(Cl)C1C#CC2=CC=CC(=C2)C1
C#CCN(CC)CN
O=C1NC=C2CCCC(Cl)=[PH]2O1
C#CCCC1=CC(C(C)NCSNC=NBr)=CC=C(I)C=N1
C#CCN(CC)CC
C=C(C#CC)C(C)=O
CC1=CC(C(=O)O)=C=[SH]1=O
O=C(Br)NC1=CC=CC(Br)=C1S(=O)(=O)CC1=C(Br)C=CC=C1F
O=C(NCC(CBr)C[PH](=O)O)SC(O)F
C1#CCNCCC1
C#CN(CC)C(=CC=CC=N)NC(=O)CN(CCC1CC1)CC1=CC=CC=C1
ClC1=C2SCPC2=CCC1
C#P(N)C(=N)C=CO
CCOC1=CC=CC=C1
CC1=C(NCN)C=CC=CC=C1F
CC1=CC=CC=C1
FC1=CC=CC=C1
COCC(=O)N1CCCC2=CC=CC=C(C1)SN=C1C=CC=CC=CC=C2C1
CC(C)CN(CC=CN=O)CCN
CCN1CCCN(Br)CC1
CN1SCCC(Cl)=CC1=CC1=CC=C(O)C=C1Cl
IC1=CC=CC=C1
C1CCNCC1
CCC(C)NC(=O)C(C)CC
O=C1CCCC(Cl)=CC2=CC=C(Cl)SCC(CN1)NC2=O
BrCC1COCC1=CC1=CCCO1
CC1=CC=CC=C1
C#CCC(C)=O
OCC1=CC(CN(CC2=CC=CC=C2)C2=CC=CC=C2)=CC=C1
CC(=O)NC1=CC=CC=CN1Br
CNC1C=CC=CC=CC=C(SC)PC2=CC1=NN2CC1CCN1
CC1=CC=CC=C1
O=C(F)OC1=CC(Cl)=CC(I)=C1
CC=CC=C1N=CC(=O)N1C=CC(=CC=C1C=CC(O)=CC=NCCCN1O)OC
CP(C)C(F)F
CC(N)C(C)PN1CCCCC2CSCCN2C1=O
CC=CC(=CC1=CCCC(C(=O)NC)C1)N=O
O=C(CCC(F)Br)CC1NC2=CC(=CC=C2)S(=O)CCSC2=CC=C1C=C2Br
O=S1(=O)C=CC(SCI)=N1
ClC1=CC=CC=C1
CC[PH]1=C(C=CC(=O)SC)O1
C1=CC=CC=C1
CNC(=CC(O)Br)C1=CC=CC(NC)C1
CC(C=CC=CC=C1C=CC=CC=[PH]1CCl)=CN(C)C
CC(C)CCCCN
CC(=O)C1=CCCC(Br)=C1
N#CCCNC(=O)N1CC2=CC=CC=C1CC=CC=CC=C2
O=C1CC2=CC=CC(=C2)OC2=CC=C(CCC(=CCSS)C=CC=C2)SC(C=C(F)Cl)=CC=CC(F)=CC=C2CCCCC2N1
C#CC(C)C(=C)O
COC(N=CNCCN)=C1C2=CNN=C1CCCCOCC2
O=CC1=CC=CC=C1
CCCCC1=CC=C(Br)N=C(CC)O1
CSCC(P)CN1CC2=CC=C(C=C2)C=C2C(F)=CC=C[PH]2=NC=CN=C(OP(F)Br)C1
CC(Br)(Br)C(P)=CC1=CC(Cl)=CC=C1C(=O)N(Cl)Br
FC1=CC=CC=C1
C#CCCCSC(=CC(Br)=CS)CCC
CCC1=CC=C(OC)C=C1
CN(N)CC(=O)C1=C(I)C=C(I)C=C1
C1=CC=CC=C1
C1#CNC=CC1
ClC1=CC=CC=C1
CC1=CC(F)=C(CCCCO)C=C1OC(N)CO
CCC(CI)CC(F)(F)Cl
CCNC(CC)CCl
O=C1CCNC=C(Cl)C=C(C(=CC=C2C=CC=C2)CCl)N1
CCCC1CCCNC1
CCNCC1=NNCS1
C#CC1C=CC=CC=C(CC)OC1
CCOC1=CC=C(I)C=C1
O=S(N1C(C=C2C=C2CCO)=C(Cl)C=C(SNF)CCC1O)C(F)(Cl)Br
CCC(C#N)C(=CN(C)CC)C1=CC(F)=C1
IC1=CC=CC=C1
NC1=CC=CC=CC=C1
C1=CC=CC=C1
CCNC(=NNC)C(C)CC(CC)CC
C1=CCC(CC2CCNC2)C1
C=CC=C1C=CC=CN(C(O)C#N)CC(CN(CC)CC)NC1=O
C#CC(=C)C(C=NC(=O)OCC=N)=NN
COC=C1C2=CC(F)=CC(=C2CCl)C1N
CCC(CC(=O)NC)CC1C=NC(=O)C1
